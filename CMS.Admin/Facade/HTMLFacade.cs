﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
   public class HTMLFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        SEOFacade _seo = new SEOFacade();
        public List<HtmlDto> GetAllHtmls()
        {
            List<HtmlDto> listdata = new List<HtmlDto>();
            var data = _uow.GetSet<Html>().ToList();
            listdata = Mapper.Map<List<HtmlDto>>(data);
            return listdata;
        }
        public ResponseDto SaveHTML(HtmlDto dto)
        {
            
            List<Language> languages = _uow.GetSet<Language>().Where(x=>x.Status==(int)EntityStatus.Published).ToList();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Html();
                    entity = _uow.GetById<Html>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<Html>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Page has been Edited Successfully";
                }
                else
                {
                    foreach (Language item in languages)
                    {
                        var entity = new Html();
                        entity = Mapper.Map<Html>(dto);
                        entity.LangId = item.Id;
                        entity.MasterId = (_uow.GetSet<Html>().Count() > 0 ? (_uow.GetSet<Html>().OrderByDescending(x => x.Id).First().MasterId + 1) : 1);
                        entity.LastUpdate = DateTime.Now;
                        _uow.Add<Html>(entity);
                        dto.Id=_uow.Save();
                        SEODto seodto = new SEODto();
                        Mapper.Map(dto, seodto);
                        _seo.SaveSEO(seodto);
                    }
                    
                    response.Success = true;
                    response.Message = "Page has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public HtmlDto LoadHTML(long id)
        {
            HtmlDto dto = new HtmlDto();
            List<ContentDto> contents = new List<ContentDto>();
            var entity = _uow.GetById<Html>(id);
            dto = Mapper.Map(entity, dto);
            var contentsdata = _uow.GetSet<Content>().Where(x => x.HtmlID == id && x.Status==(int)EntityStatus.Published).OrderBy(x => x.SortIndex);
            contents = Mapper.Map<List<ContentDto>>(contentsdata);
            dto.Contents = contents;
            return dto;
        }
        public HtmlDto LoadHTMLByMasterId(long masterid, int langid)
        {
            HtmlDto dto = new HtmlDto();
            var entity = _uow.GetSet<Html>().Where(x=>x.MasterId==masterid && x.LangId==langid);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteHTML(long id)
        {
            var entity = new Html();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Html>(id);
            entity.Status = (int)EntityStatus.Deleted;
            _uow.Update<Html>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Page has Been Removed Successfully!";
            return response;
        }
        public List<ContentDto> GetAllHtmlContents(long htmlid)
        {
            List<ContentDto> listdata = new List<ContentDto>();
            var data = _uow.GetSet<Content>().Where(x=>x.HtmlID==htmlid).ToList();
            listdata = Mapper.Map<List<ContentDto>>(data);
            return listdata;
        }
        public ContentDto LoadContent(long contentid)
        {
            ContentDto dto = new ContentDto();
            var entity = _uow.GetById<Content>(contentid);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteContent(long id)
        {
            var entity = new Content();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Content>(id);
            _uow.Remove<Content>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Content Have Been Removed Successfully!";
            return response;
        }
        public ResponseDto SaveContent(ContentDto dto)
        {
            var entity = new Content();
            var path = "";
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<Content>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                    }
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<Content>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Content>(dto);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                    }
                    entity.LastUpdate = DateTime.Now;
                    _uow.Add<Content>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public List<LanguageDto> GetAllLanguages()
        {
            List<LanguageDto> listdata = new List<LanguageDto>();
            var data = _uow.GetSet<Language>().Where(x => x.Status == (int)EntityStatus.Published).ToList();
            listdata = Mapper.Map<List<LanguageDto>>(data);
            return listdata;
        }

       
    }
}
