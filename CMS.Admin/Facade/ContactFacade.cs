﻿using CMS.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Dto;
using CMS;
using AutoMapper;
using CMS.Admin.Util;
using CMS.DAL;
using CMS.Infrastructure;

namespace CMS.Admin.Facade
{
   public class ContactFacade : BaseFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());

        public List<ContactRequestDto> GetAllContactMessages()
        {
            List<ContactRequestDto> listdata = new List<ContactRequestDto>();
            var data = _uow.GetSet<ContacRequest>().ToList();
            listdata = Mapper.Map<List<ContactRequestDto>>(data);
            return listdata;
        }
        public ResponseDto SaveContactMessage(ContactRequestDto dto)
        {
            var entity = new ContacRequest();
            ResponseDto response = new ResponseDto();
            EmailDto email = new EmailDto();
            string htmlString = @"<html>
                      <body><div>
                      <p>Dear Admin,</p>
                      <p>We Have got the reservation request from the following Person</p><br/>
                      <p>Name: " + dto.FirstName + ' '+ dto.LastName + "</p>" +
                      "<p>Phone: " + dto.Phone + "</p>" +
                      "<p>Restaurant: " + dto.BranchName + "</p>" +
                      "<p>Email: " + dto.Email + "</p>" +
                      "<p>Subject: " + dto.ContactSubjectName + "</p>" +
                      "<p>Requested Date: " + dto.ContactDateText + "</p>" +
                      "<p>Requested Time: " + dto.RequestedTime + "</p>" +
                      "<p>Message: " + dto.Message + "</p><br/>" +
                      "<p>Thank You!,<br>-Pärla Sushi Team</br></p>" +
                      "</div></body></html>";   
              
            try
            {
                email.FromAdrress = dto.Email;
                //email.CCAddress = "mayedul@optimizeab.com";
                email.ToAddress = "info@parlasushi.se";
                email.Subject = "Contact Us Request";
                email.EmailBody = htmlString;
                if ( dto.Id > 0)
                {
                    entity = _uow.GetById<ContacRequest>((long)dto.Id);
                    entity.LastUpdate = DateTime.Today;
        
                    Mapper.Map(dto, entity);
                    _uow.Update<ContacRequest>(entity);
                    response.Success = true;
                    response.Message = "Contact Message has been Successfully";
                }
                else
                {
                    entity = Mapper.Map<ContacRequest>(dto);
                    entity.LastUpdate = DateTime.Today;
                    entity.ContactDate = DateTime.Today;
                    _uow.Add<ContacRequest>(entity);
                    response.Success = true;
                    response.Message = "Thank you for Your request. We will get back to you soon";
                }
                _uow.Save();
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                    //"Contact Message Save Failed";
            }
            
            //CMSEmail.Send(email);
            return response;
        }

        public ContactRequestDto LoadMessage(long id)
        {
            ContactRequestDto dto = new ContactRequestDto();
            var entity = _uow.GetById<ContacRequest>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteMessage(long id)
        {
            var entity = new ContacRequest();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<ContacRequest>(id);
            _uow.Remove<ContacRequest>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Contact Message Has Been Removed Successfully!";
            return response;
        }
    }
}
