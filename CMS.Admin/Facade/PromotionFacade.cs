﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
   public class PromotionFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Promotion Primary
        public List<PromotionDto> GetAllPromotion()
        {
            List<PromotionDto> listdata = new List<PromotionDto>();
            var data = _uow.GetSet<Promotion>().ToList();
            listdata = Mapper.Map<List<PromotionDto>>(data);
            return listdata;
        }

        public ResponseDto SavePromotion(PromotionDto dto)
        {
            var path = "";
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Promotion();
                    entity = _uow.GetById<Promotion>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                    }
                    entity.LastUpdated = DateTime.Now;
                    _uow.Update<Promotion>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Promotion has been Edited Successfully";
                }
                else
                {
                    var entity = new Promotion();
                    entity = Mapper.Map<Promotion>(dto);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                    }
                    entity.LastUpdated = DateTime.Now;
                    _uow.Add<Promotion>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Promotion has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public PromotionDto LoadPromotion(long id)
        {
            PromotionDto dto = new PromotionDto();
            var entity = _uow.GetById<Promotion>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto DeleteList(long id)
        {
            var entity = new Promotion();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Promotion>(id);
            entity.Status = (int)EntityStatus.Deleted;
            _uow.Update<Promotion>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Promotion has Been Removed Successfully!";
            return response;
        }
        #endregion


        #region Promotional Prices
        public List<PromotionalPriceDto> GetAllPromotionPrices(long proid)
        {
            List<PromotionalPriceDto> listdata = new List<PromotionalPriceDto>();
            var data = _uow.GetSet<PromotionalPrice>().Where(x => x.PromotionId == proid).ToList();
            listdata = Mapper.Map<List<PromotionalPriceDto>>(data);
            return listdata;
        }
        public PromotionalPriceDto LoadPrice(long id)
        {
            PromotionalPriceDto dto = new PromotionalPriceDto();
            var entity = _uow.GetById<PromotionalPrice>(id);
            Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeletePrice(long id)
        {
            var entity = new PromotionalPrice();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<PromotionalPrice>(id);
            _uow.Remove<PromotionalPrice>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Promotional Price has been Removed Successfully!";
            return response;
        }
        public ResponseDto SavePrice(PromotionalPriceDto dto)
        {
            var entity = new PromotionalPrice();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<PromotionalPrice>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    entity.LatestPrice = (dto.FlatAmount > 0 ? (dto.BasePrice - dto.FlatAmount) : (dto.BasePrice - (dto.BasePrice * (dto.Discount / 100))));
                    _uow.Update<PromotionalPrice>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "PromotionalPrice has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<PromotionalPrice>(dto);
                    entity.LatestPrice = (dto.FlatAmount > 0 ? (dto.BasePrice - dto.FlatAmount) : (dto.BasePrice - (dto.BasePrice * (dto.Discount / 100))));
                    _uow.Add<PromotionalPrice>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Promotional Price has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        #endregion

        public List<ProductPromoDto> GetPromotionalProducts()
        {
            List<ProductPromoDto> listdata = new List<ProductPromoDto>();
            var promodata=_uow.GetSet<Promotion>().Where(x => x.StartDate < DateTime.Today && x.EndDate >=DateTime.Today).FirstOrDefault();
            if(promodata!=null)
            {

            }
            var pricedata = _uow.GetSet<PromotionalPrice>().Where(x => x.PromotionId==promodata.Id);
            //List<PromotionalPriceDto> pricelist = new List<PromotionalPriceDto>();
            //pricelist=Mapper.Map<List<PromotionalPriceDto>>(pricedata);
            //foreach (PromotionalPriceDto pri in pricelist)
            //{
            //    var prinfo = _uow.GetById<Product>((long)inv.ProductId);
            //    listdata.Add(Mapper.Map<ProductDto>(prinfo));
            //}
            listdata = (from pri in pricedata
                        select new ProductPromoDto
                        {
                            PromotionId = pri.PromotionId,
                            PromotionTitle=pri.Promotion.PromotionTitle,
                            PromoExpiryDate=(DateTime)pri.Promotion.EndDate,
                            ProductId=pri.ProductId,
                            ProductName=pri.Product.Name,
                            BasePrice=pri.BasePrice,
                            LatestPrice=pri.LatestPrice,
                            SmallText=pri.Product.SmallDetails,
                    
                            ImagePath= (pri.Product.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + pri.Product.ThumbImage.Replace("\\", "/")) : "")
                        }).ToList();
            return listdata;
        }
    }
}
