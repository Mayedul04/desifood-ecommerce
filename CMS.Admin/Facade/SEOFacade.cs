﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Admin.Facade
{
   public class SEOFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        public List<SEODto> GetAllSEOs()
        {
            List<SEODto> listdata = new List<SEODto>();
            var data = _uow.GetSet<SEO>().ToList();
            listdata = Mapper.Map<List<SEODto>>(data);
            return listdata;
        }
        public ResponseDto SaveSEO(SEODto dto)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new SEO();
                    entity = _uow.GetById<SEO>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<SEO>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Page SEO has been Edited Successfully";
                }
                else
                {
                    var entity = new SEO();
                    entity = Mapper.Map<SEO>(dto);
                    entity.LastUpdate = DateTime.Now;
                    _uow.Add<SEO>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Page SEO has been Have been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public SEODto LoadSEO(long id)
        {
            SEODto dto = new SEODto();
            var entity = _uow.GetById<SEO>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteSEO(long id)
        {
            var entity = new SEO();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<SEO>(id);
            _uow.Remove<SEO>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "SEO has Been Removed Successfully!";
            return response;
        }
        public SEODto LoadSEOfromPageId(PageType ptype, long pageid)
        {
            SEODto dto = new SEODto();
            SEO entity = new SEO();
            entity = _uow.GetSet<SEO>().Where(x=>x.PageType==(int)ptype && x.PageId==pageid).FirstOrDefault();
            Mapper.Map(entity, dto);
            dto.PageType = (int)ptype;
            dto.PageId = pageid;
            return dto;
        }
    }
}
