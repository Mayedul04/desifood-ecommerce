﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using CMS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Admin.Facade
{
    
    public class OrdersFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Order Master
        public List<OrderMasterDto> GetAllOrders()
        {
            List<OrderMasterDto> listdata = new List<OrderMasterDto>();
            var data = _uow.GetSet<OrderMaster>().ToList();
            listdata = Mapper.Map<List<OrderMasterDto>>(data);
            return listdata;
        }
        public ResponseDto SaveOrderMaster(OrderMasterDto dto)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new OrderMaster();
                    entity = _uow.GetById<OrderMaster>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    // Order Details are ging to be save on the same time
                    _uow.Update<OrderMaster>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Your order information has been updated Successfully!";
                }
                else
                {
                    var entity = new OrderMaster();
                    entity = Mapper.Map<OrderMaster>(dto);
                    entity.UniqueCode = UiUtil.RandomString(6);
                    entity.PayementStatus = (int)PaymentStatus.Pending;
                    entity.OrderDate = DateTime.Now;
                    // Order Details are ging to be save on the same time
                    _uow.Add<OrderMaster>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Your order has been Placed!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public OrderMasterDto LoadOrderInfo(long oid)
        {
            OrderMasterDto dto = new OrderMasterDto();
            List<OrderDetailDto> orders = new List<OrderDetailDto>();
            var entity = _uow.GetById<OrderMaster>(oid);
            Mapper.Map(entity, dto);
            orders = GetOrderDetails(oid);
            dto.OrderDetails = orders;
            return dto;
        }

        #endregion

        public List<OrderDetailDto> GetOrderDetails(long orderid)
        {
            List<OrderDetailDto> listdata = new List<OrderDetailDto>();
            var data = _uow.GetSet<OrderDetail>().Where(x=>x.OrderId==orderid).ToList();
            listdata = Mapper.Map<List<OrderDetailDto>>(data);
            return listdata;
        }
        
        
    }
}
