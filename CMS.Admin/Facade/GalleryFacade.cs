﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
  public  class GalleryFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        public List<GalleryDto> GetAllGallery()
        {
            List<GalleryDto> listdata = new List<GalleryDto>();
            var data = _uow.GetSet<Gallery>().ToList();
            listdata = Mapper.Map<List<GalleryDto>>(data);
            return listdata;
        }
        public ResponseDto SaveGallery(GalleryDto dto)
        {

            var path = "";
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Gallery();
                    entity = Mapper.Map(dto, entity);
                    entity = _uow.GetById<Gallery>((long)dto.Id);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/" + dto.Title + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/" + dto.Title + "/" + dto.ThumbImage.FileName;
                    }
                    
                    
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<Gallery>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Gallery has been Edited Successfully";
                }
                else
                {
                   
                    var entity = new Gallery();
                    entity = Mapper.Map<Gallery>(dto);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/" + dto.Title + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/" + dto.Title + "/" + dto.ThumbImage.FileName;
                    }
                    
                   
                    
                    entity.LastUpdate = DateTime.Now;
                    _uow.Add<Gallery>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Gallery has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public GalleryDto LoadGallery(long id)
        {
            GalleryDto dto = new GalleryDto();
            List<GalleryItemDto> contents = new List<GalleryItemDto>();
            var entity = _uow.GetById<Gallery>(id);
            dto = Mapper.Map(entity, dto);
            dto.GalleryItems = LoadGalleryItems(id);
            return dto;
        }

        public List<GalleryItemDto> LoadGalleryItems(long gid)
        {
            List<GalleryItemDto> items = new List<GalleryItemDto>();
            var contentsdata = _uow.GetSet<GalleryItem>().Where(x => x.GalleryId == gid);
            items = Mapper.Map<List<GalleryItemDto>>(contentsdata);
            return items;
        }

        public ResponseDto DeleteGallery(long id)
        {
            var entity = new Gallery();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Gallery>(id);
            entity.Status = (int)EntityStatus.Deleted;
            _uow.Update<Gallery>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Gallery has Been Removed Successfully!";
            return response;
        }

        public GalleryItemDto LoadImage(long imageid)
        {
            GalleryItemDto dto = new GalleryItemDto();
            var entity = _uow.GetById<GalleryItem>(imageid);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteImage(long id)
        {
            var entity = new GalleryItem();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<GalleryItem>(id);
            _uow.Remove<GalleryItem>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Image Have Been Removed Successfully!";
            return response;
        }
        public ResponseDto SaveImage(GalleryItemDto dto)
        {
            var entity = new GalleryItem();
            var path = "";
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<GalleryItem>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/" + dto.GalleryName + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "thumb_" + dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/" + dto.GalleryName + "/" + "thumb_" + dto.ThumbImage.FileName;
                    }
                    if (dto.BigImage != null)
                    {
                        path = "~/Content/UploadImages/" + dto.GalleryName + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.BigImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.BigImage.SaveAs(path);
                        entity.BigImage = "Content/UploadImages/" + dto.GalleryName + "/" + dto.BigImage.FileName;
                    }
                   
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<GalleryItem>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Image has been edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<GalleryItem>(dto);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/" + dto.GalleryName + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "thumb_" + dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/" + dto.GalleryName + "/" + "thumb_" + dto.ThumbImage.FileName;
                    }
                    if (dto.BigImage != null)
                    {
                        path = "~/Content/UploadImages/" + dto.GalleryName + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.BigImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.BigImage.SaveAs(path);
                        entity.BigImage = "Content/UploadImages/" + dto.GalleryName + "/" + dto.BigImage.FileName;
                    }
                    entity.LastUpdate = DateTime.Now;
                    _uow.Add<GalleryItem>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Image has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
    }
}
