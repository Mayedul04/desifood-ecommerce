﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Admin.Facade
{
   public class ContactSubjectFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        public List<ContactSubjectDto> GetAllContactSubjects(int? langid)
        {
            List<ContactSubjectDto> listdata = new List<ContactSubjectDto>();
            var data = _uow.GetSet<ContactSubject>().ToList();
            if (langid > 0)
                data = data.Where(x => x.LangId == langid).ToList();
            
            listdata = Mapper.Map<List<ContactSubjectDto>>(data);
            return listdata;
        }
        public ResponseDto SaveContactSubject(ContactSubjectDto dto)
        {
            var entity = new ContactSubject();
            ResponseDto response = new ResponseDto();
            try
            {

                if (dto.Id > 0)
                {
                    entity = _uow.GetById<ContactSubject>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<ContactSubject>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Subject has been edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<ContactSubject>(dto);
                    entity.LastUpdate = DateTime.Now;
                    _uow.Add<ContactSubject>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Subject has been added!";
                }

            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public ContactSubjectDto LoadContactSubject(long id)
        {
            ContactSubjectDto dto = new ContactSubjectDto();
            var entity = _uow.GetById<ContactSubject>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteSubject(long listid)
        {
            var entity = new ContactSubject();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<ContactSubject>(listid);
            _uow.Remove<ContactSubject>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Subject has been removed Successfully!";
            return response;
        }
    }
}
