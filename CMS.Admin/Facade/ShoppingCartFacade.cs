﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Admin.Facade
{
  public  class ShoppingCartFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Order Master
        public List<ShoppingCartDto> GetAllCarts()
        {
            List<ShoppingCartDto> listdata = new List<ShoppingCartDto>();
            var data = _uow.GetSet<ShoppingCart>().ToList();
            listdata = Mapper.Map<List<ShoppingCartDto>>(data);
            return listdata;
        }
        public ResponseDto SaveCart(ShoppingCartDto dto)
        {
            ResponseDto response = new ResponseDto();
            try
            { 
                if (dto.Id > 0)
                {
                    var entity = new ShoppingCart();
                    entity = _uow.GetById<ShoppingCart>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    entity.LastUpdated = DateTime.Now;
                    // Order Details are ging to be save on the same time
                    _uow.Update<ShoppingCart>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Your cart has been updated Successfully!";
                }
                else
                {
                    var entity = new ShoppingCart();
                    entity = Mapper.Map<ShoppingCart>(dto);
                    entity.LastUpdated = DateTime.Now;
                    // Order Details are ging to be save on the same time
                    _uow.Add<ShoppingCart>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Your cart has been Created!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public ShoppingCartDto LoadCart(long cartid)
        {
            ShoppingCartDto dto = new ShoppingCartDto();
            List<ShoppingCartDetailDto> orders = new List<ShoppingCartDetailDto>();
            var entity = _uow.GetById<ShoppingCart>(cartid);
            Mapper.Map(entity, dto);
            orders = GetCartDetails(cartid);
            dto.ShoppingCartDetails = orders;
            return dto;
        }

        #endregion

        public List<ShoppingCartDetailDto> GetCartDetails(long cartid)
        {
            List<ShoppingCartDetailDto> listdata = new List<ShoppingCartDetailDto>();
            var data = _uow.GetSet<ShoppingCartDetail>().Where(x => x.CartId == cartid).ToList();
            listdata = Mapper.Map<List<ShoppingCartDetailDto>>(data);
            return listdata;
        }

        public ResponseDto SaveItemToCart(ShoppingCartDetailDto dto)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new ShoppingCartDetail();
                    entity = _uow.GetById<ShoppingCartDetail>((long)dto.Id);
                    Mapper.Map(dto, entity);
                   
                    _uow.Update<ShoppingCartDetail>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Your item has been updated Successfully!";
                }
                else
                {
                    var entity = new ShoppingCartDetail();
                    entity = Mapper.Map<ShoppingCartDetail>(dto);
                   
                    _uow.Add<ShoppingCartDetail>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Your item has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
    }
}
