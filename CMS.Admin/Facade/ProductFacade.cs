﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
    public class ProductFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());

        #region Product Category
        public List<ProductCategoryDto> GetAllCategory(int level = 1)
        {
            List<ProductCategoryDto> listdata = new List<ProductCategoryDto>();
            var data = _uow.GetSet<ProductCategory>().Where(x => x.CatLevel == level).ToList();
            listdata = Mapper.Map<List<ProductCategoryDto>>(data);
            return listdata;
        }


        public ProductCategoryDto LoadCategory(long id)
        {
            ProductCategoryDto dto = new ProductCategoryDto();
            var entity = _uow.GetById<ProductCategory>(id);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveCategory(ProductCategoryDto dto)
        {
            var entity = new ProductCategory();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<ProductCategory>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    _uow.Update<ProductCategory>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Product Category has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<ProductCategory>(dto);
                    _uow.Add<ProductCategory>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Product Category has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public ResponseDto DeleteCategory(long id)
        {
            var entity = new ProductCategory();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<ProductCategory>(id);
            _uow.Remove<ProductCategory>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Product Category has Been Removed Successfully!";
            return response;
        }
        #endregion

        #region Product Category Contents
        public List<CategoryDetailDto> GetCategoryContents(long cid)
        {
            List<CategoryDetailDto> listdata = new List<CategoryDetailDto>();
            var data = _uow.GetSet<CategoryDetail>().Where(x => x.CategoryId == cid).ToList();
            listdata = Mapper.Map<List<CategoryDetailDto>>(data);
            return listdata;
        }
        public CategoryDetailDto LoadCategoryContentDetails(long contentid)
        {
            CategoryDetailDto dto = new CategoryDetailDto();
            var entity = _uow.GetById<CategoryDetail>(contentid);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveCategoryContent(CategoryDetailDto dto)
        {
            var path = "";
            var entity = new CategoryDetail();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<CategoryDetail>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.BigImage != null)
                    {
                        path = "~/Content/UploadImages/Categories/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.BigImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.BigImage.SaveAs(path);
                        entity.BigImage = "Content/UploadImages/Categories/" + dto.BigImage.FileName;
                    }

                    _uow.Update<CategoryDetail>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Category Content has been Edited successfully!";
                }
                else
                {
                    entity = Mapper.Map<CategoryDetail>(dto);
                    if (dto.BigImage != null)
                    {
                        path = "~/Content/UploadImages/Categories/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.BigImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.BigImage.SaveAs(path);
                        entity.BigImage = "Content/UploadImages/Categories/" + dto.BigImage.FileName;
                    }

                    _uow.Add<CategoryDetail>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Category Content has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public ResponseDto DeleteCategoryContent(long id)
        {
            var entity = new CategoryDetail();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<CategoryDetail>(id);
            _uow.Remove<CategoryDetail>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Category Content has been Removed successfully!";
            return response;
        }
        #endregion


        #region Master Product

        public List<ProductDto> GetAllProducts()
        {
            List<ProductDto> listdata = new List<ProductDto>();
            var data = _uow.GetSet<Product>().ToList();
            listdata = Mapper.Map<List<ProductDto>>(data);
            return listdata;
        }
        public List<ProductDto> GetAllByCategory(long category)
        {
            List<ProductDto> listdata = new List<ProductDto>();
            var data = _uow.GetSet<Product>().Where(x => x.CategoryId == category).ToList();
            listdata = Mapper.Map<List<ProductDto>>(data);
            return listdata;
        }
        public List<ProductDto> GetAllChildProductByCategory(long category)
        {
            List<ProductDto> listdata = new List<ProductDto>();
            var data = _uow.GetSet<Product>().Where(x => x.CategoryId == category || x.ProductCategory.ParentId == category).ToList();
            listdata = Mapper.Map<List<ProductDto>>(data);
            return listdata;
        }





        #region Business Logic
        public List<ProductClientDto> GetAllFeaturedProducts()
        {
            List<ProductClientDto> listdata = new List<ProductClientDto>();
            var invdata = _uow.GetSet<ProductInventory>();
            var productdata = _uow.GetSet<Product>().Where(x => x.Status == (int)EntityStatus.Published && x.IsFeatured == true);
            listdata = (from inv in invdata
                        join pr in productdata
                        on inv.ProductId equals pr.Id
                        select new ProductClientDto
                        {
                            ProductId = pr.Id,
                            Name = pr.Name,
                            CategoryId = pr.CategoryId,
                            ProductCategoryName = pr.ProductCategory.CategoryName,
                            Alias = pr.Alias,
                            BasePrice = inv.BasePrice,
                            LatestPrice = inv.BasePrice,
                            ThumbImagePath = (pr.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + pr.ThumbImage.Replace("\\", "/")) : ""),
                            UoMText = (pr.UoM > 0 ? pr.UoM1.ShortName : ""),
                            IsFeatured = (bool)pr.IsFeatured,
                            SmallDetails = pr.SmallDetails,
                            BrandName = inv.Brand.Name,
                            EntryDate = inv.EntryDate,
                            ExpiryDate = inv.ExpirayDate

                        }).ToList();

            return listdata;
        }

        public List<ProductClientDto> GetAllFeaturedProductsByCategory(long category)
        {
            List<ProductClientDto> listdata = new List<ProductClientDto>();
            var productdata = _uow.GetSet<Product>().Where(x => x.CategoryId == category || x.ProductCategory.ParentId == category).Where(y => y.IsFeatured == true).ToList();
            var invdata = _uow.GetSet<ProductInventory>();
            listdata = (from inv in invdata
                        join pr in productdata
                        on inv.ProductId equals pr.Id
                        select new ProductClientDto
                        {
                            ProductId = pr.Id,
                            Name = pr.Name,
                            CategoryId = pr.CategoryId,
                            ProductCategoryName = pr.ProductCategory.CategoryName,
                            Alias = pr.Alias,
                            BasePrice = inv.BasePrice,
                            LatestPrice = inv.BasePrice,
                            ThumbImagePath = (pr.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + pr.ThumbImage.Replace("\\", "/")) : ""),
                            UoMText = (pr.UoM > 0 ? pr.UoM1.ShortName : ""),
                            IsFeatured = (bool)pr.IsFeatured,
                            SmallDetails = pr.SmallDetails,
                            BrandName = inv.Brand.Name,
                            EntryDate = inv.EntryDate,
                            ExpiryDate = inv.ExpirayDate

                        }).ToList();
            return listdata;
        }

        public List<ProductSummaryDto> ProductCountByCategory()
        {
            List<ProductSummaryDto> listdata = new List<ProductSummaryDto>();
            var pdata = _uow.GetSet<Product>().ToList();
            var catdata = _uow.GetSet<ProductCategory>().ToList();
            listdata = (from pr in pdata
                        join cat in catdata
                        on pr.CategoryId equals cat.Id
                        group pr by new { pr.CategoryId, pr.ProductCategory }
                        into g
                        select new ProductSummaryDto
                        {
                            CategoryId = g.Key.CategoryId,
                            CategoryName = g.Key.ProductCategory.CategoryName,
                            ProductCount = g.Count(),
                            CategoryLevel = (MenuLevel)g.Key.ProductCategory.CatLevel,
                        }).ToList();

            return listdata;
        }

        public List<ProductClientDto> GetMultiPurposeProducts()
        {
            List<ProductClientDto> listdata = new List<ProductClientDto>();
            var invdata = _uow.GetSet<ProductInventory>();
            var promodata = _uow.GetSet<PromotionalPrice>().Where(x => x.Promotion.EndDate <= DateTime.Now);
            var productdata = _uow.GetSet<Product>().Where(x => x.Status == (int)EntityStatus.Published);
            listdata = (from inv in invdata
                        join pr in productdata
                        on inv.ProductId equals pr.Id
                        join promo in promodata
                        on inv.ProductId equals promo.ProductId
                        into invproductdata
                        from invprod in invproductdata.DefaultIfEmpty()
                        select new ProductClientDto
                        {
                            ProductId = pr.Id,
                            Name = pr.Name,
                            CategoryId = pr.CategoryId,
                            ProductCategoryName = pr.ProductCategory.CategoryName,
                            Alias = pr.Alias,
                            BasePrice = inv.BasePrice,
                            LatestPrice = (invprod.PromotionId > 0 ? invprod.LatestPrice : 0),
                            ThumbImagePath = (pr.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + pr.ThumbImage.Replace("\\", "/")) : ""),
                            UoMText = (pr.UoM > 0 ? pr.UoM1.ShortName : ""),
                            IsFeatured = (bool)pr.IsFeatured,
                            SmallDetails = pr.SmallDetails,
                            BrandName = inv.Brand.Name,
                            EntryDate = inv.EntryDate,
                            ExpiryDate = inv.ExpirayDate,
                            ÓnPromotion = (invprod.PromotionId > 0 ? true : false),
                            PromotionTitle = (invprod.PromotionId > 0 ? invprod.Promotion.PromotionTitle : "")
                        }
                        ).ToList();
            return listdata;
        }

        public ProductClientDto GetMultiPurposeSingleProduct(long prodid)
        {
            ProductClientDto finaldata = new ProductClientDto();
            var invdata = _uow.GetSet<ProductInventory>().Where(x => x.ProductId == prodid).OrderByDescending(x => x.Id).FirstOrDefault();
            var promodata = _uow.GetSet<PromotionalPrice>().Where(x => x.Promotion.EndDate <= DateTime.Now && x.ProductId == prodid).OrderByDescending(x => x.Id).FirstOrDefault();
            var productdata = _uow.GetById<Product>(prodid);
            finaldata = new ProductClientDto
            {
                ProductId = productdata.Id,
                Name = productdata.Name,
                CategoryId = productdata.CategoryId,
                ProductCategoryName = productdata.ProductCategory.CategoryName,
                Alias = productdata.Alias,
                BasePrice = invdata.BasePrice,
                LatestPrice = (promodata != null ? promodata.LatestPrice : invdata.BasePrice),
                ThumbImagePath = (productdata.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + productdata.ThumbImage.Replace("\\", "/")) : ""),
                UoMText = (productdata.UoM > 0 ? productdata.UoM1.ShortName : ""),
                IsFeatured = (bool)productdata.IsFeatured,
                SmallDetails = productdata.SmallDetails,
                BrandName = invdata.Brand.Name,
                EntryDate = invdata.EntryDate,
                ExpiryDate = invdata.ExpirayDate,
                ÓnPromotion = (promodata != null ? true : false),
                PromotionTitle = (promodata != null ? promodata.Promotion.PromotionTitle : "")
            };
            return finaldata;
        }

        #endregion

        public ResponseDto SaveProduct(ProductDto dto)
        {
            var path = "";
            List<Language> languages = _uow.GetSet<Language>().Where(x => x.Status == (int)EntityStatus.Published).ToList();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Product();
                    entity = _uow.GetById<Product>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/Products/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/Products/" + dto.ThumbImage.FileName;
                    }
                    entity.LastUpdated = DateTime.Now;
                    _uow.Update<Product>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Product has been Edited Successfully";
                }
                else
                {
                    foreach (Language item in languages)
                    {
                        var entity = new Product();
                        entity = Mapper.Map<Product>(dto);
                        entity.LangId = item.Id;
                        entity.MasterId = (_uow.GetSet<Product>().Count() > 0 ? (_uow.GetSet<Product>().OrderByDescending(x => x.Id).First().MasterId + 1) : 1);
                        if (dto.ThumbImage != null)
                        {
                            path = "~/Content/UploadImages/Products/";
                            string input = path.Replace("~/", "");
                            path = HttpContext.Current.Server.MapPath(path);
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            var fileName = dto.ThumbImage.FileName;
                            path = Path.Combine(path, fileName);
                            dto.ThumbImage.SaveAs(path);
                            entity.ThumbImage = "Content/UploadImages/Products/" + dto.ThumbImage.FileName;
                        }
                        entity.LastUpdated = DateTime.Now;
                        _uow.Add<Product>(entity);
                    }

                    _uow.Save();
                    response.Success = true;
                    response.Message = "Product has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public ProductDto LoadProduct(long id)
        {
            ProductDto dto = new ProductDto();
            List<ProductContentDto> contents = new List<ProductContentDto>();
            List<BlogCommentDto> comments = new List<BlogCommentDto>();
            var entity = _uow.GetById<Product>(id);
            Mapper.Map(entity, dto);
            contents = GetProductContents(id);
            dto.ProductContents = contents;
            return dto;
        }
        public List<TagDto> GetTags(long prodid)
        {
            List<TagDto> listdata = new List<TagDto>();
            //Need to implement and changing db
            //var data = _uow.GetSet<TagRelation>().Where(x => x.Counterpart==blogid).ToList();
            //listdata = Mapper.Map<List<BlogDto>>(data);
            return listdata;

        }
        public ProductDto LoadProductListByMasterId(long masterid, int langid)
        {
            ProductDto dto = new ProductDto();
            var entity = _uow.GetSet<Product>().Where(x => x.MasterId == masterid && x.LangId == langid);
            Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteProduct(long id)
        {
            var entity = new Product();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Product>(id);
            entity.Status = (int)EntityStatus.Deleted;
            _uow.Update<Product>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Product has Been Removed Successfully!";
            return response;
        }
        #endregion

        #region Product Details
        public List<ProductContentDto> GetProductContents(long pid)
        {
            List<ProductContentDto> listdata = new List<ProductContentDto>();
            var data = _uow.GetSet<ProductContent>().Where(x => x.ProductId == pid).ToList();
            listdata = Mapper.Map<List<ProductContentDto>>(data);
            return listdata;
        }
        public ProductContentDto LoadContent(long contentid)
        {
            ProductContentDto dto = new ProductContentDto();
            var entity = _uow.GetById<ProductContent>(contentid);
            Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteContent(long id)
        {
            var entity = new ProductContent();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<ProductContent>(id);
            entity.Status = (int)EntityStatus.Deleted;
            entity.LastUpdated = DateTime.Now;
            _uow.Update<ProductContent>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Content has been Removed Successfully!";
            return response;
        }
        public ResponseDto SaveContent(ProductContentDto dto)
        {
            var path = "";
            var entity = new ProductContent();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<ProductContent>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/Products/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/Products/" + dto.Image.FileName;
                    }
                    entity.LastUpdated = DateTime.Now;
                    _uow.Update<ProductContent>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<ProductContent>(dto);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/Products/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/Products/" + dto.Image.FileName;
                    }
                    entity.LastUpdated = DateTime.Now;
                    _uow.Add<ProductContent>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        #endregion

        #region Product Inventory
        public List<ProductInventoryDto> GetAllInventory(long? catid)
        {
            List<ProductInventoryDto> listdata = new List<ProductInventoryDto>();

            if (catid != null)
            {
                var data = _uow.GetSet<ProductInventory>().Where(x => x.Product.CategoryId == catid);
                listdata = Mapper.Map<List<ProductInventoryDto>>(data);
            }
            else
            {
                var data = _uow.GetSet<ProductInventory>();
                listdata = Mapper.Map<List<ProductInventoryDto>>(data);
            }
            return listdata;
        }
        public ProductInventoryDto getInventory(long productid)
        {
            ProductInventoryDto dto = new ProductInventoryDto();
            var entity = _uow.GetSet<ProductInventory>().Where(x=>x.ProductId==productid).OrderByDescending(x=>x.Id).FirstOrDefault();
            Mapper.Map(entity, dto);
            return dto;
        }

        public ProductInventoryDto LoadInventory(long inventid)
        {
            ProductInventoryDto dto = new ProductInventoryDto();
            var entity = _uow.GetById<ProductInventory>(inventid);
            Mapper.Map(entity, dto);
            return dto;
        }


        public ResponseDto SaveInventory(ProductInventoryDto dto)
        {
            var entity = new ProductInventory();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<ProductInventory>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    _uow.Update<ProductInventory>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Inventory has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<ProductInventory>(dto);
                    _uow.Add<ProductInventory>(entity);
                    entity.EntryDate = DateTime.Now;
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Inventory has been Added!";
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }



        public List<ProductClientDto> GetFreshArrivals()
        {
            List<ProductClientDto> listdata = new List<ProductClientDto>();
            var invdata = _uow.GetSet<ProductInventory>().Where(x => System.Data.Entity.DbFunctions.AddDays(x.EntryDate,3) > DateTime.Today);
            var productdata = _uow.GetSet<Product>().Where(x => x.Status == (int)EntityStatus.Published);
            listdata = (from inv in invdata
                        join pr in productdata
                        on inv.ProductId equals pr.Id
                        select new ProductClientDto
                        {
                            ProductId = pr.Id,
                            Name=pr.Name,
                            CategoryId=pr.CategoryId,
                            ProductCategoryName=pr.ProductCategory.CategoryName,
                            Alias=pr.Alias,
                            BasePrice=inv.BasePrice,
                            LatestPrice=inv.BasePrice,
                            ThumbImagePath=(pr.ThumbImage!=null? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + pr.ThumbImage.Replace("\\", "/")):""),
                            UoMText=(pr.UoM > 0 ? pr.UoM1.ShortName : ""),
                            IsFeatured=(bool)pr.IsFeatured,
                            SmallDetails=pr.SmallDetails,
                            BrandName=inv.Brand.Name,
                            EntryDate=inv.EntryDate,
                            ExpiryDate=inv.ExpirayDate

                        }).ToList();
                   

            return listdata;
        }

        #endregion

        #region Product Stock Master
        public List<ProductStockMasterDto> AllProductStock()
        {
            List<ProductStockMasterDto> listdata = new List<ProductStockMasterDto>();
            var data = _uow.GetSet<ProductStockMaster>().ToList();
            listdata = Mapper.Map<List<ProductStockMasterDto>>(data);
            return listdata;
        }
        public ProductStockMasterDto StockByProduct(long prodid)
        {
            ProductStockMasterDto dto = new ProductStockMasterDto();
            var entity = _uow.GetById<ProductStockMaster>(prodid);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveStockMaster(ProductStockMasterDto dto)
        {
            var entity = new ProductStockMaster();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<ProductStockMaster>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    entity.LastUpdated = DateTime.Now;
                    _uow.Update<ProductStockMaster>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Stock has been Updated Successfully";
                }
                //else
                //{
                //    entity = Mapper.Map<ProductStockMaster>(dto);
                //    _uow.Add<ProductStockMaster>(entity);
                //    _uow.Save();
                //    response.Success = true;
                //    response.Message = "Inventory has been Added!";
                //}
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }


        #region Business Logic
        public List<CategorySummaryDto> GeCategoryStockSummary()
        {
            List<CategorySummaryDto> listdata = new List<CategorySummaryDto>();
            var stockdata = _uow.GetSet<ProductStockMasterDto>().ToList();
            var productdata = _uow.GetSet<Product>().ToList();
            listdata = (from st in stockdata
                        join pr in productdata
                        on st.ProductId equals pr.Id
                        group st by new { pr.CategoryId, pr.ProductCategory }
                        into g
                        select new CategorySummaryDto
                        {
                            CategoryId = g.Key.CategoryId,
                            CategoryName = g.Key.ProductCategory.CategoryName,
                            InventoryCount = g.Sum(x => (int)x.CurrentStock),
                            CategoryLevel = (MenuLevel)g.Key.ProductCategory.CatLevel,
                            ParentId = (long)g.Key.ProductCategory.ParentId,
                            ParentName = g.Key.ProductCategory.ProductCategory2.CategoryName
                        }).ToList();

            return listdata;
        } 
        #endregion

        #endregion

        #region Stock Log
        public List<ProductStockOutLogDto> GetStockLog(long masterid)
        {
            List<ProductStockOutLogDto> listdata = new List<ProductStockOutLogDto>();
            var data = _uow.GetSet<ProductStockOutLog>().Where(x => x.MasterId == masterid).OrderByDescending(y => y.EffectDate).ToList();
            listdata = Mapper.Map<List<ProductStockOutLogDto>>(data);
            return listdata;
        }
        public ProductStockOutLogDto LogDetails(long logid)
        {
            ProductStockOutLogDto dto = new ProductStockOutLogDto();
            var entity = _uow.GetById<ProductStockOutLog>(logid);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveLog(ProductStockOutLogDto dto)
        {
            var entity = new ProductStockOutLog();
            ResponseDto response = new ResponseDto();
            try
            {

                entity = Mapper.Map<ProductStockOutLog>(dto);
                entity.EffectDate = DateTime.Now;
                _uow.Add<ProductStockOutLog>(entity);
                _uow.Save();
                response.Success = true;
                response.Message = "Log has been saved!";

            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        #endregion

        public List<BrandDto> GetBrandsByProduct(long prodid)
        {
            List<BrandDto> listdata = new List<BrandDto>();
            var data = _uow.GetSet<ProductInventory>().Where(x => x.ProductId == prodid).ToList();
            listdata = (from d in data
                        group d by new { d.Brand}
                        into g
                        select new BrandDto
                       { 
                            Name= g.Key.Brand.Name,
                            Id= g.Key.Brand.Id
                
                      }).ToList();
            return listdata;
        }
    }
}
