﻿using CMS.Dto;
using CMS.DAL;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace CMS.Admin.Facade
{
    public class TagFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        public List<TagDto> GetAllTags(TagType? type)
        {
            List<TagDto> listdata = new List<TagDto>();
            var data = _uow.GetSet<Tag>().Where(x=>x.Type==(int)type).ToList();
            listdata = Mapper.Map<List<TagDto>>(data);
            return listdata;
        }
        public ResponseDto SaveTag(TagDto dto)
        {
            var entity = new Tag();
            ResponseDto response = new ResponseDto();
            try
            {

                if (dto.Id > 0)
                {
                    entity = _uow.GetById<Tag>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    _uow.Update<Tag>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Tag has been edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Tag>(dto);
                    _uow.Add<Tag>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Tag has been Added!";
                }
               
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public TagDto LoadTag(long listid)
        {
            TagDto dto = new TagDto();
            var entity = _uow.GetById<Tag>(listid);
            dto = Mapper.Map(entity,dto);
            return dto;
        }
        public ResponseDto DeleteTag(long listid)
        {
            var entity = new Tag();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Tag>(listid);
            _uow.Remove<Tag>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Tag has been removed Successfully!";
            return response;
        }
    }
}
