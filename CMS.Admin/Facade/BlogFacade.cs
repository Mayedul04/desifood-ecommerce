﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
   public class BlogFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Master List
        public List<BlogDto> GetAllLists()
        {
            List<BlogDto> listdata = new List<BlogDto>();
            var data = _uow.GetSet<Blog>().ToList();
            listdata = Mapper.Map<List<BlogDto>>(data);
            return listdata;
        }
        public ResponseDto SaveList(BlogDto dto)
        {
            var path = "";
            List<Language> languages = _uow.GetSet<Language>().Where(x => x.Status == (int)EntityStatus.Published).ToList();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Blog();
                    entity = _uow.GetById<Blog>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/" + dto.ThumbImage.FileName;
                    }
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<Blog>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Blog has been Edited Successfully";
                }
                else
                {
                    foreach (Language item in languages)
                    {
                        var entity = new Blog();
                        entity = Mapper.Map<Blog>(dto);
                        entity.LangId = item.Id;
                        entity.MasterId = (_uow.GetSet<Blog>().Count() > 0 ? (_uow.GetSet<Blog>().OrderByDescending(x => x.Id).First().MasterId + 1) : 1);
                        if (dto.ThumbImage != null)
                        {
                            path = "~/Content/UploadImages/";
                            string input = path.Replace("~/", "");
                            path = HttpContext.Current.Server.MapPath(path);
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            var fileName = dto.ThumbImage.FileName;
                            path = Path.Combine(path, fileName);
                            dto.ThumbImage.SaveAs(path);
                            entity.ThumbImage = "Content/UploadImages/" + dto.ThumbImage.FileName;
                        }
                        entity.LastUpdate = DateTime.Now;
                        _uow.Add<Blog>(entity);
                    }

                    _uow.Save();
                    response.Success = true;
                    response.Message = "Blog has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public BlogDto LoadList(long id)
        {
            BlogDto dto = new BlogDto();
            List<BlogContentDto> contents = new List<BlogContentDto>();
            List<BlogCommentDto> comments = new List<BlogCommentDto>();
            var entity = _uow.GetById<Blog>(id);
            Mapper.Map(entity, dto);
            contents = GetAllListContents(id);
           // comments = GetAllBlogComments(id);
            dto.BlogContents = contents;
           // dto.BlogComments = comments;
            return dto;
        }
        public List<TagDto> GetTags(long blogid)
        {
            List<TagDto> listdata = new List<TagDto>();
            //Need to implement and changing db
            //var data = _uow.GetSet<TagRelation>().Where(x => x.Counterpart==blogid).ToList();
            //listdata = Mapper.Map<List<BlogDto>>(data);
            return listdata;

        }
        public BlogDto LoadListByMasterId(long masterid, int langid)
        {
            BlogDto dto = new BlogDto();
            var entity = _uow.GetSet<Blog>().Where(x => x.MasterId == masterid && x.LangId == langid);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteList(long id)
        {
            var entity = new Blog();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Blog>(id);
            entity.Status = (int)EntityStatus.Deleted;
            _uow.Update<Blog>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Blog has Been Removed Successfully!";
            return response;
        }
        #endregion
        #region List Contents
        public List<BlogContentDto> GetAllListContents(long lid)
        {
            List<BlogContentDto> listdata = new List<BlogContentDto>();
            var data = _uow.GetSet<BlogContent>().Where(x => x.BlogId == lid).ToList();
            listdata = Mapper.Map<List<BlogContentDto>>(data);
            return listdata;
        }
        public BlogContentDto LoadContent(long contentid)
        {
            BlogContentDto dto = new BlogContentDto();
            var entity = _uow.GetById<BlogContent>(contentid);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteContent(long id)
        {
            var entity = new BlogContent();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<BlogContent>(id);
            entity.Status = (int)EntityStatus.Deleted;
            entity.LastUpdate = DateTime.Now;
            _uow.Update<BlogContent>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Content has been Removed Successfully!";
            return response;
        }
        public ResponseDto SaveContent(BlogContentDto dto)
        {
            var path = "";
            var entity = new BlogContent();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<BlogContent>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                    }
                    entity.LastUpdate = DateTime.Now;
                    _uow.Update<BlogContent>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<BlogContent>(dto);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                    }
                    entity.LastUpdate = DateTime.Now;
                    _uow.Add<BlogContent>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        #endregion

        #region Blog Comments
        public List<BlogCommentDto> GetAllBlogComments(long blogid)
        {
            List<BlogCommentDto> listdata = new List<BlogCommentDto>();
            var data = _uow.GetSet<BlogComment>().Where(x => x.BlogId == blogid).ToList();
            listdata = Mapper.Map<List<BlogCommentDto>>(data);
            return listdata;
        }
        public BlogCommentDto LoadComment(long commentid)
        {
            BlogCommentDto dto = new BlogCommentDto();
            var entity = _uow.GetById<BlogComment>(commentid);
            dto = Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto DeleteComment(long id)
        {
            var entity = new BlogComment();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<BlogComment>(id);
            _uow.Remove<BlogComment>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Comment has been Removed Successfully!";
            return response;
        }

        public ResponseDto SaveContent(BlogCommentDto dto)
        {
            var entity = new BlogComment();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<BlogComment>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    _uow.Update<BlogComment>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Comment has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<BlogComment>(dto);
                    _uow.Add<BlogComment>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Comment has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        } 
        #endregion
    }
}
