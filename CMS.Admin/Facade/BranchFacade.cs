﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
   public class BranchFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Branch Primary
        public List<BranchDto> GetAllLists()
        {
            List<BranchDto> listdata = new List<BranchDto>();
            var data = _uow.GetSet<Branch>().ToList();
            listdata = Mapper.Map<List<BranchDto>>(data);
            return listdata;
        }
        public ResponseDto SaveList(BranchDto dto)
        {
            var path = "";
            List<Language> languages = _uow.GetSet<Language>().Where(x => x.Status == (int)EntityStatus.Published).ToList();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Branch();
                    entity = _uow.GetById<Branch>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                    }
                    entity.LastUpdated = DateTime.Now;
                    _uow.Update<Branch>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Blog has been Edited Successfully";
                }
                else
                {
                    foreach (Language item in languages)
                    {
                        var entity = new Branch();
                        entity = Mapper.Map<Branch>(dto);
                        entity.LangId = item.Id;
                        entity.MasterId = (_uow.GetSet<Branch>().Count() > 0 ? (_uow.GetSet<Branch>().OrderByDescending(x => x.Id).First().MasterId + 1) : 1);
                        if (dto.Image != null)
                        {
                            path = "~/Content/UploadImages/";
                            string input = path.Replace("~/", "");
                            path = HttpContext.Current.Server.MapPath(path);
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            var fileName = dto.Image.FileName;
                            path = Path.Combine(path, fileName);
                            dto.Image.SaveAs(path);
                            entity.Image = "Content/UploadImages/" + dto.Image.FileName;
                        }
                        entity.LastUpdated = DateTime.Now;
                        _uow.Add<Branch>(entity);
                    }

                    _uow.Save();
                    response.Success = true;
                    response.Message = "Branch has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public BranchDto LoadList(long id)
        {
            BranchDto dto = new BranchDto();
            List<OpeningHourDto> hours = new List<OpeningHourDto>();

            var entity = _uow.GetById<Branch>(id);
            Mapper.Map(entity, dto);
            hours = GetAllOpeningHours(id);
            dto.OpeningHours = hours;
            return dto;
        }

        public BranchDto LoadListByMasterId(long masterid, int langid)
        {
            BranchDto dto = new BranchDto();
            var entity = _uow.GetSet<Branch>().Where(x => x.MasterId == masterid && x.LangId == langid);
            Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteList(long id)
        {
            var entity = new Branch();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Branch>(id);
            entity.Status = (int)EntityStatus.Deleted;
            _uow.Update<Branch>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Branch has Been removed Successfully!";
            return response;
        }
        #endregion

        #region Opening Hours
        public List<OpeningHourDto> GetAllOpeningHours(long brid)
        {
            List<OpeningHourDto> listdata = new List<OpeningHourDto>();
            var data = _uow.GetSet<OpeningHour>().Where(x => x.BranchId == brid).ToList();
            listdata = Mapper.Map<List<OpeningHourDto>>(data);
            return listdata;
        }
        public OpeningHourDto LoadOpeningHours(long id)
        {
            OpeningHourDto dto = new OpeningHourDto();
            var entity = _uow.GetById<OpeningHour>(id);
            Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteHours(long id)
        {
            var entity = new OpeningHour();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<OpeningHour>(id);
            _uow.Remove<OpeningHour>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Openning hour has been Removed Successfully!";
            return response;
        }
        public ResponseDto SaveOpeningHour(OpeningHourDto dto)
        {
            var entity = new OpeningHour();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<OpeningHour>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    _uow.Update<OpeningHour>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Opening Hour has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<OpeningHour>(dto);
                    _uow.Add<OpeningHour>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Opening Hour has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        #endregion

        #region Team Members
        public List<TeamDto> GetAllTeamMembers(long? brid)
        {
            List<TeamDto> listdata = new List<TeamDto>();
            List<Team> data = new List<Team>();
            if (brid>0)
             data = _uow.GetSet<Team>().Where(x => x.BranchId == brid).ToList();
            else
             data = _uow.GetSet<Team>().ToList();
            listdata = Mapper.Map<List<TeamDto>>(data);
            return listdata;
        }
        public TeamDto LoadProfile(long id)
        {
            TeamDto dto = new TeamDto();
            var entity = _uow.GetById<Team>(id);
            Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto RemoveTeamMember(long id)
        {
            var entity = new Team();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Team>(id);
            _uow.Remove<Team>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Team Member has been Removed Successfully!";
            return response;
        }
        public ResponseDto SaveTeamMember(TeamDto dto)
        {
            var path = "";
            var entity = new Team();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<Team>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.Photo != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Photo.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Photo.SaveAs(path);
                        entity.Photo = "Content/UploadImages/" + dto.Photo.FileName;
                    }
                    entity.LastUpdated = DateTime.Today;
                    _uow.Update<Team>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Team Member has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Team>(dto);
                    if (dto.Photo != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Photo.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Photo.SaveAs(path);
                        entity.Photo = "Content/UploadImages/" + dto.Photo.FileName;
                    }
                    entity.LastUpdated = DateTime.Today;
                    _uow.Add<Team>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Team Member has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        } 
        #endregion
    }
}
