﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using CMS.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
   public class CustomerFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());

        ShoppingCartFacade _scart = new ShoppingCartFacade();

        #region Customers
        public List<CustomerDto> GetAllCustomers()
        {
            List<CustomerDto> listdata = new List<CustomerDto>();
            var data = _uow.GetSet<Customer>().ToList();
            listdata = Mapper.Map<List<CustomerDto>>(data);
            return listdata;
        }
        public ResponseDto SaveCustomer(CustomerDto dto)
        {
            var path = "";
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Customer();
                    entity = _uow.GetById<Customer>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/Customers/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/Customers/" + dto.Image.FileName;
                    }
                    _uow.Update<Customer>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Your information has been updated Successfully!";
                }
                else
                {
                    var entity = new Customer();
                    entity = Mapper.Map<Customer>(dto);
                    if (dto.Image != null)
                    {
                        path = "~/Content/UploadImages/Customers/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Image.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Image.SaveAs(path);
                        entity.Image = "Content/UploadImages/Customers/" + dto.Image.FileName;
                    }
                    entity.VarificationCode = UiUtil.RandomString(10);
                    entity.IsVarified = true;

                    _uow.Add<Customer>(entity);
                    _uow.Save();
                    ShoppingCartDto wishcartdto = new ShoppingCartDto();
                    wishcartdto.CustomerId = entity.Id;
                    wishcartdto.GrandTotal = 0;
                    wishcartdto.Status = CartStatus.Wished;
                    _scart.SaveCart(wishcartdto);
                    ShoppingCartDto maincartdto = new ShoppingCartDto();
                    maincartdto.CustomerId = entity.Id;
                    maincartdto.GrandTotal = 0;
                    maincartdto.Status = CartStatus.Main;
                    _scart.SaveCart(maincartdto);
                    response.Success = true;
                    response.Message = "Customer has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

       
        public CustomerDto LoadCustomerDetails(long id)
        {
            CustomerDto dto = new CustomerDto();
            List<OrderMasterDto> orders = new List<OrderMasterDto>();
            List<ShoppingCartDto> shoppingcarts = new List<ShoppingCartDto>();
            var entity = _uow.GetById<Customer>(id);
            Mapper.Map(entity, dto);
            //contents = GetAllListContents(id);
            // comments = GetAllBlogComments(id);
            //dto.BlogContents = contents;
            // dto.BlogComments = comments;
            return dto;
        }
        
        public ResponseDto DeleteCustomer(long id)
        {
            var entity = new Customer();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Customer>(id);
            //entity.Status = (int)EntityStatus.Deleted;
            _uow.Remove<Customer>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Customer has Been Removed Successfully!";
            return response;
        }

        public CustomerDto DoLogin(CustomerDto dto)
        {
            CustomerDto vm = new CustomerDto();
            var users = _uow.GetSet<Customer>();
            var user = _uow.GetSet<Customer>().FirstOrDefault(x => x.UserName == dto.UserName && x.Password == dto.Password);
            var carts = _uow.GetSet<ShoppingCart>().Where(x => x.CustomerId == user.Id);
            if (user != null)
            {
                vm.Id = user.Id;
                vm.UserName = user.UserName;
                vm.ImagePath= (user.Image != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + user.Image.Replace("\\", "/")) : "");
                vm.MainCartId =  carts.Where(y=>y.Status==(int)CartStatus.Main).FirstOrDefault().Id;
                vm.WishCartId = carts.Where(y => y.Status == (int)CartStatus.Wished).FirstOrDefault().Id;
            }

            return vm;

        }
        #endregion
    }
}
