﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Admin.Facade
{
    
    public class DeliveryScheduleFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Schedule
        public List<DeliverySlotDto> GetAllSlots()
        {
            List<DeliverySlotDto> listdata = new List<DeliverySlotDto>();
            var data = _uow.GetSet<DeliverySlot>().ToList();
            listdata = Mapper.Map<List<DeliverySlotDto>>(data);
            return listdata;
        }

        public DeliverySlotDto LoadSlotDetails(long id)
        {
            DeliverySlotDto dto = new DeliverySlotDto();
            var entity = _uow.GetById<DeliverySlot>(id);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveBrand(DeliverySlotDto dto)
        {
            var entity = new DeliverySlot();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<DeliverySlot>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    _uow.Update<DeliverySlot>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Slot has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<DeliverySlot>(dto);
                    _uow.Add<DeliverySlot>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Slot has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public ResponseDto DeleteSlot(long id)
        {
            var entity = new DeliverySlot();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<DeliverySlot>(id);
            _uow.Remove<DeliverySlot>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Slot has Been Removed Successfully!";
            return response;
        }
        #endregion

        #region Customer Schedule
        public List<CustomerDeliveryScheduleDto> GetAllBookedSchedules(long? slotid)
        {
            var data = new List<CustomerDeliverySchedule>();
            List<CustomerDeliveryScheduleDto> listdata = new List<CustomerDeliveryScheduleDto>();
            if(slotid!=null)
                data = _uow.GetSet<CustomerDeliverySchedule>().Where(x=>x.SlotId==slotid).ToList();
            else
                data = _uow.GetSet<CustomerDeliverySchedule>().ToList();
            listdata = Mapper.Map<List<CustomerDeliveryScheduleDto>>(data);
            return listdata;
        }

        public CustomerDeliveryScheduleDto LoadBookingDetails(long id)
        {
            CustomerDeliveryScheduleDto dto = new CustomerDeliveryScheduleDto();
            var entity = _uow.GetById<CustomerDeliverySchedule>(id);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveCustomerDeleverySchedule(CustomerDeliveryScheduleDto dto)
        {
            var entity = new CustomerDeliverySchedule();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<CustomerDeliverySchedule>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    _uow.Update<CustomerDeliverySchedule>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Schedule has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<CustomerDeliverySchedule>(dto);
                    _uow.Add<CustomerDeliverySchedule>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Schedule has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public ResponseDto DeleteSchedule(long id)
        {
            var entity = new CustomerDeliverySchedule();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<CustomerDeliverySchedule>(id);
            _uow.Remove<CustomerDeliverySchedule>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Schedule has Been Removed Successfully!";
            return response;
        }
        #endregion
    }
}
