﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Facade;
using CMS.Dto;
using CMS.DAL;
using AutoMapper;
using CMS.Infrastructure;

namespace CMS.Admin.Facade
{
   public class AdminUsersFacade : BaseFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        public List<AdminPanelLoginsDto> GetAllUsers()
        {
            List<AdminPanelLoginsDto> listdata = new List<AdminPanelLoginsDto>();
            var data = _uow.GetSet<AdminPanelLogin>().Where(i => i.Status == (int)EntityStatus.Published).ToList();
            listdata = Mapper.Map<List<AdminPanelLoginsDto>>(data);
            return listdata;
        }
        public ResponseDto SaveAdminUser(AdminPanelLoginsDto dto)
        {
            var entity = new AdminPanelLogin();
            ResponseDto response = new ResponseDto();
            try
            {

                if (dto.Id > 0)
                {
                    entity = _uow.GetById<AdminPanelLogin>((long)dto.Id);
                    entity = Mapper.Map(dto, entity);
                    entity.LastUpdate = DateTime.Now;
                    entity.Status = (int)EntityStatus.Published;
                    _uow.Update<AdminPanelLogin>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "User Information has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<AdminPanelLogin>(dto);
                    entity.Status = (int)EntityStatus.Published;
                    entity.LastUpdate = DateTime.Now;
                    _uow.Add<AdminPanelLogin>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "User Information has been Added!";
                }

            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public AdminPanelLoginsDto LoadUserInfo(long userid)
        {
            AdminPanelLoginsDto dto = new AdminPanelLoginsDto();
            var entity = _uow.GetById<AdminPanelLogin>(userid);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteUser(long userid)
        {
            var entity = new AdminPanelLogin();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<AdminPanelLogin>(userid);
            entity.Status = (int)EntityStatus.Deleted;
            entity.LastUpdate = DateTime.Now;
            _uow.Update<AdminPanelLogin>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "User Information has been Removed Successfully!";
            return response;
        }
        public UserResourceDto DoLogin(AdminPanelLoginsDto dto)
        {
            UserResourceDto vm = new UserResourceDto();
            var users = _uow.GetSet<AdminPanelLogin>();
            var user = _uow.GetSet<AdminPanelLogin>().FirstOrDefault(x => x.UN_1 == dto.UN_1 && x.PS_1 == dto.PS_1);
            if (user != null) 
            {
                vm.UserId = user.Id;
                vm.UserName = user.UN_1;
                vm.IsAdmin = (user.Role == (int)RoleEnum.Admin ? true : false);
                
            }
            
            return vm;

        }
    }
}
