﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
   public class TestimonialFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Testimonials
        public List<TestimonialDto> GetAllTestimonials()
        {
            List<TestimonialDto> listdata = new List<TestimonialDto>();
            var data = _uow.GetSet<Testimonial>().ToList();
            listdata = Mapper.Map<List<TestimonialDto>>(data);
            return listdata;
        }

        public ResponseDto SaveTestimonial(TestimonialDto dto)
        {
            var path = "";
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    var entity = new Testimonial();
                    entity = _uow.GetById<Testimonial>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.Photo != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Photo.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Photo.SaveAs(path);
                        entity.Photo = "Content/UploadImages/" + dto.Photo.FileName;
                    }
                    entity.CommentDate = DateTime.Now;
                    _uow.Update<Testimonial>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Testimonial has been Edited Successfully";
                }
                else
                {
                    var entity = new Testimonial();
                    entity = Mapper.Map<Testimonial>(dto);
                    if (dto.Photo != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.Photo.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Photo.SaveAs(path);
                        entity.Photo = "Content/UploadImages/" + dto.Photo.FileName;
                    }
                    entity.CommentDate = DateTime.Now;
                    _uow.Add<Testimonial>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Testimonial has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public TestimonialDto LoadTestimonial(long id)
        {
            TestimonialDto dto = new TestimonialDto();
            var entity = _uow.GetById<Testimonial>(id);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto DeleteTestimonial(long id)
        {
            var entity = new Testimonial();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Testimonial>(id);
            _uow.Remove<Testimonial>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Testimonial has Been Removed Successfully!";
            return response;
        }
        #endregion

    }
}
