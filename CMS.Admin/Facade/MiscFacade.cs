﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Admin.Facade
{
  public  class MiscFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        #region Language
        public List<LanguageDto> GetAllLanguages()
        {
            List<LanguageDto> listdata = new List<LanguageDto>();
            var data = _uow.GetSet<Language>().Where(x => x.Status == (int)EntityStatus.Published).ToList();
            listdata = Mapper.Map<List<LanguageDto>>(data);
            return listdata;
        }
        public ResponseDto SaveLanguage(LanguageDto dto)
        {
            var entity = new Language();
            var path = "";
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<Language>((long)dto.Id);
                    if (dto.IconImage != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.IconImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.IconImage.SaveAs(path);
                        entity.IconImage = "Content/UploadImages/" + dto.IconImage.FileName;
                    }

                    entity = Mapper.Map(dto, entity);


                    _uow.Update<Language>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Language has been Edited Successfully";
                }
                else
                {
                    if (dto.IconImage != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.IconImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.IconImage.SaveAs(path);
                        entity.IconImage = "Content/UploadImages/" + dto.IconImage.FileName;
                    }
                    entity = Mapper.Map<Language>(dto);

                    _uow.Add<Language>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Language Has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public LanguageDto LoadLanguage(long id)
        {
            LanguageDto dto = new LanguageDto();
            var entity = _uow.GetById<Language>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        #endregion

        #region Contact Subjects
        public List<ContactSubjectDto> GetAllContactSubjects()
        {
            List<ContactSubjectDto> listdata = new List<ContactSubjectDto>();
            var data = _uow.GetSet<ContactSubject>().ToList();
            listdata = Mapper.Map<List<ContactSubjectDto>>(data);
            return listdata;
        }
        public ContactSubjectDto LoadContactSubject(long id)
        {
            ContactSubjectDto dto = new ContactSubjectDto();
            var entity = _uow.GetById<ContactSubject>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveContactSubject(ContactSubjectDto dto)
        {
            var entity = new ContactSubject();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<ContactSubject>((long)dto.Id);

                    entity = Mapper.Map(dto, entity);
                    entity.LastUpdate = DateTime.Today;

                    _uow.Update<ContactSubject>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Contact Subject has been Edited Successfully";
                }
                else
                {

                    entity = Mapper.Map<ContactSubject>(dto);
                    entity.LastUpdate = DateTime.Today;
                    _uow.Add<ContactSubject>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Contact Subject has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public ResponseDto DeleteSubject(long id)
        {
            var entity = new ContactSubject();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<ContactSubject>(id);
            _uow.Remove<ContactSubject>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Contact Subject has Been Removed Successfully!";
            return response;
        }
        #endregion

        #region Content Type
        public List<ContentTypeDto> GetAllContentTypes(string category)
        {
            var data = new List<ContentType>();
            List<ContentTypeDto> listdata = new List<ContentTypeDto>();
            if(category!="")
                 data = _uow.GetSet<ContentType>().Where(y=>y.Category==category).ToList();
            else
                 data = _uow.GetSet<ContentType>().ToList();
            listdata = Mapper.Map<List<ContentTypeDto>>(data);
            return listdata;
        }

        public ContentTypeDto LoadContentType(long id)
        {
            ContentTypeDto dto = new ContentTypeDto();
            var entity = _uow.GetById<ContentType>(id);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveContentType(ContentTypeDto dto)
        {
            var entity = new ContentType();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<ContentType>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    entity.LastUpdate = DateTime.Today;
                    _uow.Update<ContentType>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content Type has been Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<ContentType>(dto);
                    entity.LastUpdate = DateTime.Today;
                    _uow.Add<ContentType>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Content Type has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public ResponseDto DeleteContentType(long id)
        {
            var entity = new ContentType();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<ContentType>(id);
            entity.Status = (int)EntityStatus.Deleted;
            entity.LastUpdate = DateTime.Now;
            _uow.Update<ContentType>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Content Type has Been Removed Successfully!";
            return response;
        }
        #endregion

        #region Brands
        public List<BrandDto> GetAllBrands(int? typeId)
        {
            var data = new List<Brand>();
            List<BrandDto> listdata = new List<BrandDto>();
            if (typeId != null)
                data = _uow.GetSet<Brand>().Where(y => y.TypeId == typeId).ToList();
            else
                data = _uow.GetSet<Brand>().ToList();
            listdata = Mapper.Map<List<BrandDto>>(data);
            return listdata;
        }

        public BrandDto LoadBrandDetails(long id)
        {
            BrandDto dto = new BrandDto();
            var entity = _uow.GetById<Brand>(id);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveBrand(BrandDto dto)
        {
            var path = "";
            var entity = new Brand();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<Brand>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/" + dto.ThumbImage.FileName;
                    }
                    _uow.Update<Brand>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Brand has been Edited successfully";
                }
                else
                {
                    entity = Mapper.Map<Brand>(dto);
                    if (dto.ThumbImage != null)
                    {
                        path = "~/Content/UploadImages/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = dto.ThumbImage.FileName;
                        path = Path.Combine(path, fileName);
                        dto.ThumbImage.SaveAs(path);
                        entity.ThumbImage = "Content/UploadImages/" + dto.ThumbImage.FileName;
                    }
                    _uow.Add<Brand>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Brand has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public ResponseDto DeleteBrand(long id)
        {
            var entity = new Brand();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Brand>(id);
            _uow.Remove<Brand>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Brand has been Removed successfully!";
            return response;
        }
        #endregion

        #region Unit of Measurement
        public List<UoMDto> GetAllUoMs()
        {
            List<UoMDto> listdata = new List<UoMDto>();
            var data = _uow.GetSet<UoM>().ToList();
            listdata = Mapper.Map<List<UoMDto>>(data);
            return listdata;
        }
        public UoMDto LoadUoM(long id)
        {
            UoMDto dto = new UoMDto();
            var entity = _uow.GetById<UoM>(id);
            Mapper.Map(entity, dto);
            return dto;
        }

        public ResponseDto SaveUoM(UoMDto dto)
        {
            var entity = new UoM();
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.Id > 0)
                {
                    entity = _uow.GetById<UoM>((long)dto.Id);
                    Mapper.Map(dto, entity);
                    _uow.Update<UoM>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Unit of Measurement has been Edited successfully!";
                }
                else
                {
                    entity = Mapper.Map<UoM>(dto);
                    _uow.Add<UoM>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Unit of Measurement has been Added!";
                }
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }
        public ResponseDto DeleteUoM(long id)
        {
            var entity = new UoM();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<UoM>(id);
            _uow.Remove<UoM>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Unit of Measurement has Been Removed successfully!";
            return response;
        } 
        #endregion
    }
}
