﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
  public  class EmailDto
    {
        public string ToAddress { get; set; }
        public string FromAdrress { get; set; }
        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public string CCAddress { get; set; }

    }
}
