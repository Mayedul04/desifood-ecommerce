﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class CategoryDetailDto
    {
        public long Id { get; set; }
        public long CategoryId { get; set; }
        public string SubTitle { get; set; }
        public string SmallDetails { get; set; }
        public string BigDetails { get; set; }
        public HttpPostedFileBase BigImage { get; set; }
        public string BigImagePath { get; set; }
        public string ImageAltText { get; set; }
        public string ProductCategoryName { get; set; }
    }
}
