﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class AdminPanelLoginsDto
    {
        public long Id { get; set; }
        public string PS_1 { get; set; }
        public string UN_1 { get; set; }
        public string Email { get; set; }
        public RoleEnum Role { get; set; }
        public Nullable<int> Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
    }
}
