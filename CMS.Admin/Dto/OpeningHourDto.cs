﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class OpeningHourDto
    {
        public int Id { get; set; }
        public string MealName { get; set; }
        public string DayText { get; set; }
        public string TimingText { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public Nullable<int> MasterId { get; set; }
        public Nullable<int> LangId { get; set; }

        public string BranchName { get; set; }
        public string LanguageName { get; set; }
    }
}
