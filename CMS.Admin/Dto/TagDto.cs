﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class TagDto
    {
        public long Id { get; set; }
        public string Tag1 { get; set; }
        public Nullable<int> Type { get; set; }
        public string TypeName { get; set; }
    }
}
