﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
  public  class ProductCategoryDto
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public Nullable<long> ParentId { get; set; }
        public GalleryLevel CatLevel { get; set; }
        public string CategoryLevel { get; set; }
        public HttpPostedFileBase ThumbImage { get; set; }
        public string ThumbImagePath { get; set; }

        public string ParentCategory { get; set; }
        public virtual IList<CategoryDetailDto> CategoryDetails { get; set; }
        
        public virtual IList<ProductDto> Products { get; set; }
    }
}
