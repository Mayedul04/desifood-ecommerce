﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class UserResourceDto
    {
        public long UserId { get; internal set; }
        public bool IsAdmin { get; internal set; }
        public string UserName { get; set; }
        public string ApiKey { get; set; }
        public string Email { get; set; }
    }
}
