﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class SEODto
    {
        public long Id { get; set; }
        public int PageType { get; set; }

        public string PageTypeName { get; set; }
        public long PageId { get; set; }
        public string SEOTitle { get; set; }
        public string SEODescription { get; set; }
        public string Keyword { get; set; }
        public Nullable<bool> Robot { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public Nullable<int> LangId { get; set; }

        public string LanguageName { get; set; }
    }
}
