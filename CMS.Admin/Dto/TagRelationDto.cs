﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class TagRelationDto
    {
        public long Id { get; set; }
        public long TagId { get; set; }
        public string TagName { get; set; }
        public long Counterpart { get; set; }
        public string CounterPartName { get; set; }
        public TagType Type { get; set; }
        public string TypeName { get; set; }
    }
}
