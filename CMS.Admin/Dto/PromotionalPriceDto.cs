﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class PromotionalPriceDto
    {
        public long Id { get; set; }
        public long PromotionId { get; set; }
        public long ProductId { get; set; }
        public long CategoryId { get; set; }
        public Nullable<double> BasePrice { get; set; }
        public Nullable<double> Discount { get; set; }
        public Nullable<double> FlatAmount { get; set; }
        public Nullable<double> LatestPrice { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string PromotionTitle { get; set; }
    }
}
