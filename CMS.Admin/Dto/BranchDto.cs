﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class BranchDto
    {
        public int Id { get; set; }
        public string BranchName { get; set; }
        public string SmallDetails { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImagePath { get; set; }
        public string Details { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string GoogleMap { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<long> MasterId { get; set; }
        public Nullable<int> LangId { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public Nullable<int> Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdatedText { get; set; }

        public string LanguageName { get; set; }
        
        public virtual IList<ContactRequestDto> ContacRequests { get; set; }
        
        public virtual IList<OpeningHourDto> OpeningHours { get; set; }
        
        public virtual IList<TeamDto> Teams { get; set; }
    }
}
