﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class DeliverySlotDto
    {
        public long Id { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public string StartTimeText { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public string EndTimeText { get; set; }
        public Nullable<double> Capacity { get; set; }
        public virtual IList<CustomerDeliveryScheduleDto> CustomerDeliverySchedules { get; set; }
    }
}
