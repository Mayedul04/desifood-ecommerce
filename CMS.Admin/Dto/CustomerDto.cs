﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class CustomerDto
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string POBox { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImagePath { get; set; }
        public string UserName { get; set; }
        public string VarificationCode { get; set; }
        public Nullable<bool> IsVarified { get; set; }
        public long MainCartId { get; set; }
        public long WishCartId { get; set; }

        public virtual IList<OrderMasterDto> OrderMasters { get; set; }
        
        public virtual IList<ShoppingCartDto> ShoppingCarts { get; set; }
    }
}
