﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class OrderDetailDto
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long ProductId { get; set; }
        public Nullable<float> Price { get; set; }
        public Nullable<double> Qty { get; set; }
        public string OrderMasterNumber { get; set; }
        public string ProductName { get; set; }
    }
}
