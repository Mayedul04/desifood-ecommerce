﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Dto
{
   public class ProductContentDto
    {
        public long Id { get; set; }
        public Nullable<long> ProductId { get; set; }
        public Nullable<long> ContentType { get; set; }
        public string ContentTypeName { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        [AllowHtml]
        public string Details { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImagePath { get; set; }
        public string ImageAltText { get; set; }
        public Nullable<long> GalleryId { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdateText { get; set; }
        public string ProductName { get; set; }
    }
}
