﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class GalleryDto
    {
        public long Id { get; set; }
        public Nullable<long> ParentId { get; set; }
        public Nullable<int> GalleryLevel { get; set; }
        public string Title { get; set; }
        public HttpPostedFileBase ThumbImage { get; set; }
        public string ThumbImagePath { get; set; }
        public string ImageAltText { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public virtual IList<GalleryDto> ChildGallery { get; set; }
        public string ParentName { get; set; }
        public virtual IList<GalleryItemDto> GalleryItems { get; set; }
    }
}
