﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Dto
{
   public class ContentDto
    {
        public long Id { get; set; }
        public long HtmlID { get; set; }
        public Nullable<long> ContentType { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string SmallText { get; set; }
        [AllowHtml]
        public string BigText { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImagePath { get; set; }
        public string Link { get; set; }
        public Nullable<long> GalleryId { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public EntityStatus Status { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public string HtmlTitle { get; set; }
        public string StatusText { get; set; }
        public string GalleryName { get; set; }
    }
}
