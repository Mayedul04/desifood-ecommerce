﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class BlogCommentDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Avater { get; set; }
        public string Comment { get; set; }
        public Nullable<long> BlogId { get; set; }
        public Nullable<long> ReplyOf { get; set; }
        public string BlogTitle { get; set; }
    }
}
