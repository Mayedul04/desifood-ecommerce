﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class SEOPageTypeDto
    {
        public long PageId { get; set; }
        public PageType PageType { get; set; }
        public int LangId { get; set; }
    }
}
