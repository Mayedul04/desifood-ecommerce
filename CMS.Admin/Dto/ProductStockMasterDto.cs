﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ProductStockMasterDto
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public Nullable<double> CurrentStock { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdatedText { get; set; }
        public int UpdatedBy { get; set; }
        public string  ProductName { get; set; }
        
        public virtual IList<ProductStockOutLogDto> ProductStockOutLogs { get; set; }
    }
}
