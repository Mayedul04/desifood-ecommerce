﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Dto
{
   public class BlogContentDto
    {
        public long Id { get; set; }
        public Nullable<long> BlogId { get; set; }
        public Nullable<long> ContentType { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImagePath { get; set; }
        public string ImageAltText { get; set; }
        public string SmallText { get; set; }
        [AllowHtml]
        public string BigText { get; set; }
        public int SortIndex { get; set; }
        public string ExternalLink { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public Nullable<long> GalleryId { get; set; }
        public string GalleryName { get; set; }
        public string BlogName { get; set; }
    }
}
