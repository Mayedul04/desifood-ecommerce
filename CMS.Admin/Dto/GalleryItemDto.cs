﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class GalleryItemDto
    {
        public long Id { get; set; }
        public long GalleryId { get; set; }
        public string Title { get; set; }
        public HttpPostedFileBase ThumbImage { get; set; }
        public string ThumbImagePath { get; set; }
        public HttpPostedFileBase BigImage { get; set; }
        public string BigImagePath { get; set; }
        public string ImageAltText { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public string GalleryName { get; set; }
    }
}
