﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ProductSummaryDto
    {
        public MenuLevel CategoryLevel { get; set; }
        public int ProductCount { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }

    }
}
