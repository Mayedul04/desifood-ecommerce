﻿
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class HtmlDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public long MasterId { get; set; }
        public Nullable<int> LangId { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }   
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public virtual IList<ContentDto> Contents { get; set; }
        public virtual IList<SEODto> SEOs { get; set; }
        public string LanguageText { get; set; }
    }
}
