﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Dto
{
   public class TestimonialDto
    {
        public long Id { get; set; }
        public string Person { get; set; }
        public string Designation { get; set; }
        public string Company { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public string PhotoPath { get; set; }
        public string Email { get; set; }
        [AllowHtml]
        public string TestimonialText { get; set; }
        public Nullable<System.DateTime> CommentDate { get; set; }
        public string CommentDateText { get; set; }
        public Nullable<int> SortIndex { get; set; }

        public TestimonialType TestimonialType { get; set; }
        public string TestimonialTypeName { get; set; }
        public Nullable<long> ProductId { get; set; }
        
        public Nullable<int> Ratings { get; set; }
        public Nullable<bool> IsAnonymous { get; set; }

        public string ProductName { get; set; }
    }
}
