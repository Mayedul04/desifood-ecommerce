﻿
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Dto
{
    public class ContactRequestDto
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public long SubjectId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string RequestedTime { get; set; }
        public string Message { get; set; }
        public EmailAction Action { get; set; }
        public string ActionName { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public Nullable<System.DateTime> ContactDate { get; set; }
        public string ContactDateText { get; set; }
        public string LastUpdateText { get; set; }
        public string ContactSubjectName { get; set; }
        public string BranchName { get; set; }


    }
}