﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ContentTypeDto
    {
        public long Id { get; set; }
        public string Category { get; set; }
        public Nullable<int> Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
    }
}
