﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ProductStockOutLogDto
    {
        public long Id { get; set; }
        public long MasterId { get; set; }
        public long ProductId { get; set; }
        public Nullable<double> StockOut { get; set; }
        public Nullable<System.DateTime> EffectDate { get; set; }
        public string EffectDateText { get; set; }
        public Nullable<long> OrderId { get; set; }
        public string ProductName { get; set; }
    }
}
