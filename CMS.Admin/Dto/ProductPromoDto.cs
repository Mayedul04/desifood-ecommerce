﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Dto
{
    public class ProductPromoDto
    {
        public long PromotionId { get; set; }
        public long ProductId { get; set; }
        public string ImagePath { get; set; }
        public string SmallText { get; set; }
        public Nullable<double> BasePrice { get; set; }
        public Nullable<double> LatestPrice { get; set; }
        public DateTime PromoExpiryDate { get; set; }
        public string ProductName { get; set; }
        public string PromotionTitle { get; set; }
    }
}