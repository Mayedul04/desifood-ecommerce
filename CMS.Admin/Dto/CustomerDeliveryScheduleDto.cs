﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class CustomerDeliveryScheduleDto
    {
        public long Id { get; set; }
        public long SlotId { get; set; }
        public long OrderId { get; set; }
        public string DeliverySlotDetails { get; set; }
       
    }
}
