﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class UoMDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public virtual IList<ProductDto> Products { get; set; }
    }
}
