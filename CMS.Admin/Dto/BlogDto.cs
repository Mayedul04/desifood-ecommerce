﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class BlogDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string SmallDetails { get; set; }
        public HttpPostedFileBase ThumbImage { get; set; }
        public string ThumbImagePath { get; set; }
        public string ImageAltText { get; set; }
        public string Author { get; set; }
        public Nullable<System.DateTime> PublishedDate { get; set; }
        public string PublishedDateText { get; set; }
        public Nullable<long> MasterId { get; set; }
        public Nullable<int> LangId { get; set; }
        public string LanguageName { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }
        public int SortIndex { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public virtual IList<BlogContentDto> BlogContents { get; set; }
        public virtual IList<BlogCommentDto> BlogComments { get; set; }
    }
}
