﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ProductClientDto
    {
        public long ProductId { get; set; }
        public long CategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string SmallDetails { get; set; }
        public string BigDetails { get; set; }
        public string ThumbImagePath { get; set; }
        public Nullable<long> UoM { get; set; }
        public string UoMText { get; set; }
        public Nullable<double> BasePrice { get; set; }
        public Nullable<double> LatestPrice { get; set; }
        public Nullable<double> CalcultedPrice { get; set; }
        public string BrandName { get; set; }
        public string PromotionTitle { get; set; }
        public Boolean IsFeatured { get; set; }
        public bool ÓnPromotion { get; set; }
        public Nullable<DateTime> EntryDate { get; set; }
        public Nullable<DateTime> ExpiryDate { get; set; }
        public virtual IList<ProductContentDto> ProductContents { get; set; }
        public virtual IList<TestimonialDto> Testimonials { get; set; }
        public virtual IList<BrandDto> Brands { get; set; }
    }
}
