﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class LanguageDto
    {
        public int Id { get; set; }
        public string IconImagepath { get; set; }
        public HttpPostedFileBase IconImage { get; set; }
        public string LanguageName { get; set; }
        public string ShortName { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }
    }
}
