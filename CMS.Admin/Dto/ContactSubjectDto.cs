﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ContactSubjectDto
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public Nullable<int> LangId { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public string LastUpdateText { get; set; }
        public virtual IList<ContactRequestDto> ContacRequests { get; set; }
        public string LanguageName { get; set; }
    }
}
