﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Dto
{
    public class ResponseDto
    {
        public ResponseDto()
        {
            Success = false;
            Message = "";
        }

        public long? Id { get; set; }

        public String Complementary { get; set; }
        public bool? Success { get; set; }
        public string Message { get; set; }
    }
}