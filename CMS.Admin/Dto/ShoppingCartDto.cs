﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ShoppingCartDto
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public Nullable<float> GrandTotal { get; set; }
        public Nullable<float> Discount { get; set; }
        public CartStatus Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdateText { get; set; }

        public string CustomerName { get; set; }
        
        public virtual IList<ShoppingCartDetailDto> ShoppingCartDetails { get; set; }
    }
}
