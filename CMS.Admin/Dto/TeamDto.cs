﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
  public  class TeamDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public string PhotoPath { get; set; }
        public string SmallDetails { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> MasterId { get; set; }
        public Nullable<int> LangId { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public Nullable<int> Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastÚpdatedText { get; set; }

        public string BranchName { get; set; }
        public string LanguageName { get; set; }
    }
}
