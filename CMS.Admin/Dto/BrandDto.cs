﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class BrandDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public HttpPostedFileBase ThumbImage { get; set; }
        public string ThumbImagePath { get; set; }
        public string WebSite { get; set; }
        public BrandType TypeId { get; set; }
        public string TypeName { get; set; }

    }
}
