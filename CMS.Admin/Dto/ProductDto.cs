﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Dto
{
   public class ProductDto
    {
        public long Id { get; set; }
        public long CategoryId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string SmallDetails { get; set; }
        [AllowHtml]
        public string BigDetails { get; set; }
        public HttpPostedFileBase ThumbImage { get; set; }
        public string ThumbImagePath { get; set; }
        public Nullable<long> GalleryId { get; set; }
        public Nullable<long> UoM { get; set; }
        public string UoMText { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }
        public Boolean IsFeatured { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdateText { get; set; }
        public Nullable<long> MasterId { get; set; }
        public Nullable<int> LangId { get; set; }
        public string LanguageName { get; set; }
        public string ProductCategoryName { get; set; }
        public virtual IList<ProductContentDto> ProductContents { get; set; }
        public virtual IList<ProductInventoryDto> ProductInventories { get; set; }
        public virtual IList<ProductStockMasterDto> ProductStockMasters { get; set; }
        public virtual IList<PromotionalPriceDto> PromotionalPrices { get; set; }
        public virtual IList<TestimonialDto> Testimonials { get; set; }
    }
}
