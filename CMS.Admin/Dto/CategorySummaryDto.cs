﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class CategorySummaryDto
    {
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int InventoryCount { get; set; }
        public MenuLevel CategoryLevel { get; set; }
        public long ParentId { get; set; }
        public string ParentName { get; set; }
    }
}
