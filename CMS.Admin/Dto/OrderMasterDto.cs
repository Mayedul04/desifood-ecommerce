﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class OrderMasterDto
    {
        public long Id { get; set; }
        public string UniqueCode { get; set; }
        public long CartId { get; set; }
        public Nullable<long> CustomerId { get; set; }
        public Nullable<float> BilledAmount { get; set; }
        public PaymentStatus PayementStatus { get; set; }
        public string PaymentStatusText { get; set; }
        public Nullable<int> PaymentBy { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public string OrderDateText { get; set; }

        public string CustomerName { get; set; }
        
        public virtual IList<CustomerDeliveryScheduleDto> CustomerDeliverySchedules { get; set; }
        
        public virtual IList<OrderDetailDto> OrderDetails { get; set; }
    }
}
