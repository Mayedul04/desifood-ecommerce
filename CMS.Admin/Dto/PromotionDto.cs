﻿using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Dto
{
   public class PromotionDto
    {
        public long Id { get; set; }
        public string PromotionTitle { get; set; }
        public string SmallDetails { get; set; }
        public PromotionType PromotionType { get; set; }
        public string TypeName { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImagePath { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string StartDateText { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string EndDateText { get; set; }
        public EntityStatus Status { get; set; }
        public string StatusText { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdatedText { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public virtual IList<PromotionalPriceDto> PromotionalPrices { get; set; }
    }
}
