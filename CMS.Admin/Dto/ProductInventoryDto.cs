﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class ProductInventoryDto
    {
        public long Id { get; set; }
        public Nullable<long> ProductId { get; set; }
        public Nullable<double> BasePrice { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public string EntryDateText { get; set; }
        public Nullable<double> Quantity { get; set; }
        public Nullable<long> UoM { get; set; }

        public string UoMText { get; set; }
        public Nullable<System.DateTime> ExpirayDate { get; set; }
        public string ExpiryDateText { get; set; }
        public Nullable<long> BrandId { get; set; }
        public string BrandName { get; set; }
        public string ProductName { get; set; }
    }
}
