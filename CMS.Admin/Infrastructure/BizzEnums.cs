﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure
{

    public enum RoleEnum {[Display(Name = "Normal User")] User =1, [Display(Name = "CMS Admin")] Admin = 3, [Display(Name = "Super User")] SuperUser = -2 }
   
    
    public enum EmailAction {[Display(Name = "Received")] Received = 1, [Display(Name = "Processing")] Processing =2, [Display(Name = "Replied")] Replied = 3, [Display(Name = "Ignored")] Ignored =4 }
    

    public enum EntityStatus {[Display(Name = "Deleted")] Deleted = -1, [Display(Name = "Draft")] Draft = 1, [Display(Name = "Published")] Published = 2, [Display(Name = "Inactive")] Inactive =3}

    public enum PageType {[Display(Name = "Html Page")] Html = 1, [Display(Name = "List")] List = 2, [Display(Name = "Gallery")] Gallery = 3 }

    public enum TagType { Blog = 1, Menu = 2, Other }
    public enum BrandType {[Display(Name = "Buyers")] Buyers = 1, [Display(Name = "Principal Suppliers")] Supplier = 2, [Display(Name = "Vendors")] Vendor = 3 }
    public enum Gender { Male = 1, Female = 2 }
    public enum MaritalStatus { Married = 1, UnMarried = 2, Other }
    public enum EmploymentStatus { Permanent = 1, Contractual = 2, Temporary = 3 }
    public enum BloodGroup { O_Positive = 1, O_Negative, A_Positive, A_Negative, AB_Positive, AB_Negetive, B_Positive, B_Negative }
    public enum Religion { Islam = 1, Hindu = 2, Buddist, Christian, Other }
    public enum Month { January = 1, February, March, April, May, June, July, August, September, October, November, December }

    public enum GalleryLevel {[Display(Name = "Base")] Parent0 = 1, [Display(Name = "Level 1")] Level1, [Display(Name = "Level 2")] Level2, [Display(Name = "Level 3")] Level3, [Display(Name = "Level 4")] Level4 }

    public enum MenuLevel {[Display(Name = "Primary")] Level1 = 1, [Display(Name = "Secondary")] Level2, [Display(Name = "Level 3")] Level3, [Display(Name = "Level 4")] Level4}

    public enum PaymentStatus
    {
        Pending = 1, Processing = 2, Paid = 3
    }

    public enum TestimonialType { General = 1, Product = 2, Service = 3 }

    public enum CartStatus {[Display(Name = "Wish List")] Wished = 1, [Display(Name = "Main Cart")] Main = 2 }

    public enum PromotionType {[Display(Name = "Discount")] Discount = 1, [Display(Name = "Sales")] Sales = 2, [Display(Name = "Clearing")] Clearing = 3 }
}