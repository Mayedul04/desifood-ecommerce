﻿using AutoMapper;
using CMS.Dto;
using CMS.DAL;
using System;
using System.Globalization;
using CMS.Util;
using CMS.Infrastructure;


namespace CMS.AutoMaps
{
    public class CMSMappingProfile : Profile
    {
        protected override void Configure()
        {
            //base.Configure();
            CreateMap<AdminPanelLogin, AdminPanelLoginsDto>()
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "")); ;
            CreateMap<AdminPanelLoginsDto, AdminPanelLogin>();

            CreateMap<Blog, BlogDto>()
               .ForMember(d => d.ThumbImage, d => d.Ignore())
               .ForMember(d => d.ThumbImagePath, o => o.MapFrom(s => s.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.ThumbImage.Replace("\\", "/")) : ""))
               .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
               .ForMember(d => d.LanguageName, o => o.MapFrom(s => s.LangId > 0 ? s.Language.LanguageName : ""))
               .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""))
               .ForMember(d => d.PublishedDateText, o => o.MapFrom(s => s.PublishedDate != null ? ((DateTime)s.PublishedDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<BlogDto, Blog>()
                .ForMember(d => d.ThumbImage, o => o.Ignore())
                .ForMember(d => d.BlogContents, o => o.Ignore())
                .ForMember(d => d.BlogComments, o => o.Ignore());

            CreateMap<BlogContent, BlogContentDto>()
                .ForMember(d => d.GalleryName, o => o.MapFrom(s => s.GalleryId > 0 ? s.Gallery.Title : ""))
                .ForMember(d => d.Image, d => d.Ignore())
                .ForMember(d => d.ImagePath, o => o.MapFrom(s => s.Image != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Image.Replace("\\", "/")) : ""))
                .ForMember(d => d.BlogName, o => o.MapFrom(s => s.Id > 0 ? s.Blog.Title : ""))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<BlogContentDto, BlogContent>()
                .ForMember(d => d.Image, d => d.Ignore());

            CreateMap<BlogComment, BlogCommentDto>()
              .ForMember(d => d.BlogTitle, o => o.MapFrom(s => s.BlogId > 0 ? s.Blog.Title : ""));
            CreateMap<BlogCommentDto, BlogComment>();

            CreateMap<Branch, BranchDto>()
                .ForMember(d => d.Image, d => d.Ignore())
                .ForMember(d => d.ImagePath, o => o.MapFrom(s => s.Image != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Image.Replace("\\", "/")) : ""))
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LanguageName, o => o.MapFrom(s => s.LangId > 0 ? s.Language.LanguageName : ""))
                .ForMember(d => d.LastUpdatedText, o => o.MapFrom(s => s.LastUpdated != null ? ((DateTime)s.LastUpdated).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<BranchDto, Branch>()
                .ForMember(d => d.Image, o => o.Ignore())
                .ForMember(d => d.ContacRequests, o => o.Ignore())
                .ForMember(d => d.OpeningHours, o => o.Ignore())
                .ForMember(d => d.Teams, o => o.Ignore());

            CreateMap<Brand, BrandDto>()
                .ForMember(d => d.ThumbImage, d => d.Ignore())
                .ForMember(d => d.ThumbImagePath, o => o.MapFrom(s => s.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.ThumbImage.Replace("\\", "/")) : ""))
                .ForMember(d => d.TypeName, o => o.MapFrom(s => s.TypeId > 0 ? UiUtil.GetDisplayName((BrandType)s.TypeId) : "Vendor"));
            CreateMap<BrandDto, Brand>()
                .ForMember(d => d.ThumbImage, o => o.Ignore());

            CreateMap<CategoryDetail, CategoryDetailDto>()
                .ForMember(d => d.ProductCategoryName, o => o.MapFrom(s => s.CategoryId > 0 ? s.ProductCategory.CategoryName : ""))
                .ForMember(d => d.BigImage, d => d.Ignore())
                .ForMember(d => d.BigImagePath, o => o.MapFrom(s => s.BigImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.BigImage.Replace("\\", "/")) : ""));
            CreateMap<CategoryDetailDto, CategoryDetail>()
                .ForMember(d => d.BigImage, d => d.Ignore());


            CreateMap<ContacRequest, ContactRequestDto>()
                .ForMember(d => d.ContactSubjectName, o => o.MapFrom(s => s.SubjectId > 0 ? s.ContactSubject.Subject : ""))
                .ForMember(d => d.BranchName, o => o.MapFrom(s => s.BranchId > 0 ? s.Branch.BranchName : ""))
                .ForMember(d => d.ActionName, o => o.MapFrom(s => s.Action > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Action) : "Deleted"))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""))
                .ForMember(d => d.ContactDateText, o => o.MapFrom(s => s.ContactDate != null ? ((DateTime)s.ContactDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ContactRequestDto, ContacRequest>();

            CreateMap<ContactSubject, ContactSubjectDto>()
                .ForMember(d => d.LanguageName, o => o.MapFrom(s => s.LangId > 0 ? s.Language.LanguageName : ""))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ContactSubjectDto, ContactSubject>()
                .ForMember(d => d.ContacRequests, o => o.Ignore());

            CreateMap<ContentType, ContentTypeDto>()
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ContentTypeDto, ContentType>();

            CreateMap<Content, ContentDto>()
               .ForMember(d => d.GalleryName, o => o.MapFrom(s => s.GalleryId > 0 ? s.Gallery.Title : ""))
               .ForMember(d => d.Image, d => d.Ignore())
               .ForMember(d => d.ImagePath, o => o.MapFrom(s => s.Image != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Image.Replace("\\", "/")) : ""))
               .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
               .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ContentDto, Content>()
                .ForMember(d => d.Image, o => o.Ignore());

            CreateMap<Customer, CustomerDto>()
              .ForMember(d => d.Image, d => d.Ignore())
              .ForMember(d => d.ImagePath, o => o.MapFrom(s => s.Image != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Image.Replace("\\", "/")) : ""));
            CreateMap<CustomerDto, Customer>()
                .ForMember(d => d.Image, o => o.Ignore())
                .ForMember(d => d.OrderMasters, o => o.Ignore())
                .ForMember(d => d.ShoppingCarts, o => o.Ignore());

            CreateMap<CustomerDeliverySchedule, CustomerDeliveryScheduleDto>()
              .ForMember(d => d.DeliverySlotDetails, o => o.MapFrom(s => s.SlotId > 0 ? s.DeliverySlot.StartTime.ToString() : ""));
            CreateMap<CustomerDeliveryScheduleDto, CustomerDeliverySchedule>();

            CreateMap<DeliverySlot, DeliverySlotDto>()
                .ForMember(d => d.StartTimeText, o => o.MapFrom(s => s.StartTime != null ? ((DateTime)s.StartTime).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""))
                .ForMember(d => d.EndTimeText, o => o.MapFrom(s => s.EndTime != null ? ((DateTime)s.EndTime).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<DeliverySlotDto, DeliverySlot>()
                .ForMember(d => d.CustomerDeliverySchedules, o => o.Ignore());

            CreateMap<Gallery, GalleryDto>()
                .ForMember(d => d.ThumbImage, d => d.Ignore())
                .ForMember(d => d.ThumbImagePath, o => o.MapFrom(s => s.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.ThumbImage.Replace("\\", "/")) : ""))
                .ForMember(d => d.ParentName, o => o.MapFrom(s => s.ParentId > 0 ? s.Gallery2.Title : "This is Base"))
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<GalleryDto, Gallery>()
                .ForMember(d => d.ThumbImage, o => o.Ignore())
                .ForMember(d => d.Gallery1, o => o.Ignore())
                .ForMember(d => d.BlogContents, o => o.Ignore())
                .ForMember(d => d.Contents, o => o.Ignore())
                .ForMember(d => d.GalleryItems, o => o.Ignore());

            CreateMap<GalleryItem, GalleryItemDto>()
                 .ForMember(d => d.ThumbImage, d => d.Ignore())
                 .ForMember(d => d.BigImage, d => d.Ignore())
                 .ForMember(d => d.ThumbImagePath, o => o.MapFrom(s => s.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.ThumbImage.Replace("\\", "/")) : ""))
                 .ForMember(d => d.BigImagePath, o => o.MapFrom(s => s.BigImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.BigImage.Replace("\\", "/")) : ""))
                .ForMember(d => d.GalleryName, o => o.MapFrom(s => s.GalleryId > 0 ? s.Gallery.Title : ""))
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<GalleryItemDto, GalleryItem>()
                 .ForMember(d => d.ThumbImage, d => d.Ignore())
                 .ForMember(d => d.BigImage, d => d.Ignore());

            CreateMap<Html, HtmlDto>()
                .ForMember(d => d.LanguageText, o => o.MapFrom(s => s.LangId > 0 ? s.Language.LanguageName : ""))
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<HtmlDto, Html>()
                .ForMember(d => d.Contents, o => o.Ignore());

            CreateMap<Language, LanguageDto>()
                .ForMember(d => d.IconImage, d => d.Ignore())
                .ForMember(d => d.IconImagepath, o => o.MapFrom(s => s.IconImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.IconImage.Replace("\\", "/")) : ""))
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"));
            CreateMap<LanguageDto, Language>()
                .ForMember(d => d.IconImage, o => o.Ignore())
                .ForMember(d => d.ContactSubjects, o => o.Ignore())
                .ForMember(d => d.Htmls, o => o.Ignore())
                .ForMember(d => d.Blogs, o => o.Ignore());

            CreateMap<OpeningHour, OpeningHourDto>()
               .ForMember(d => d.BranchName, o => o.MapFrom(s => s.BranchId > 0 ? s.Branch.BranchName : ""))
               .ForMember(d => d.LanguageName, o => o.MapFrom(s => s.LangId > 0 ? s.Language.LanguageName : ""));
            CreateMap<OpeningHourDto, OpeningHour>();

            CreateMap<OrderMaster, OrderMasterDto>()
                .ForMember(d => d.CustomerName, o => o.MapFrom(s => s.CustomerId > 0 ? (s.Customer.FirstName + " " + s.Customer.LastName) : ""))
                .ForMember(d => d.PaymentStatusText, o => o.MapFrom(s => s.PayementStatus > 0 ? UiUtil.GetDisplayName((PaymentStatus)s.PayementStatus) : "Pending"))
                .ForMember(d => d.OrderDateText, o => o.MapFrom(s => s.OrderDate != null ? ((DateTime)s.OrderDate).ToString("dd MMM, yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<OrderMasterDto, OrderMaster>()
                .ForMember(d => d.CustomerDeliverySchedules, o => o.Ignore())
                .ForMember(d => d.OrderDetails, o => o.Ignore());

            CreateMap<OrderDetail, OrderDetailDto>()
                .ForMember(d => d.ProductName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.Name : ""))
                .ForMember(d => d.OrderMasterNumber, o => o.MapFrom(s => s.OrderId > 0 ? s.OrderMaster.UniqueCode : ""));
            CreateMap<OrderDetailDto, OrderDetail>();

            CreateMap<ProductCategory, ProductCategoryDto>()
                .ForMember(d => d.ThumbImage, d => d.Ignore())
                .ForMember(d => d.ThumbImagePath, o => o.MapFrom(s => s.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.ThumbImage.Replace("\\", "/")) : ""))
                .ForMember(d => d.ParentCategory, o => o.MapFrom(s => s.ParentId > 0 ? s.ProductCategory2.CategoryName : "This is Base"))
                .ForMember(d => d.CategoryLevel, o => o.MapFrom(s => s.CatLevel > 0 ? UiUtil.GetDisplayName((GalleryLevel)s.CatLevel) : "Base"));
            CreateMap<ProductCategoryDto, ProductCategory>()
                .ForMember(d => d.ThumbImage, o => o.Ignore())
                .ForMember(d => d.CategoryDetails, o => o.Ignore())
                .ForMember(d => d.Products, o => o.Ignore());

            CreateMap<ProductContent, ProductContentDto>()
                .ForMember(d => d.Image, d => d.Ignore())
                .ForMember(d => d.ProductName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.Name : ""))
                .ForMember(d => d.ImagePath, o => o.MapFrom(s => s.Image != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Image.Replace("\\", "/")) : ""))
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdated != null ? ((DateTime)s.LastUpdated).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ProductContentDto, ProductContent>()
                .ForMember(d => d.Image, o => o.Ignore());

            CreateMap<Product, ProductDto>()
               .ForMember(d => d.ThumbImage, d => d.Ignore())
               .ForMember(d => d.ThumbImagePath, o => o.MapFrom(s => s.ThumbImage != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.ThumbImage.Replace("\\", "/")) : ""))
               .ForMember(d => d.ProductCategoryName, o => o.MapFrom(s => s.CategoryId > 0 ? s.ProductCategory.CategoryName : ""))
               .ForMember(d => d.UoMText, o => o.MapFrom(s => s.UoM > 0 ? s.UoM1.ShortName : ""))
               .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"));
            CreateMap<ProductDto, Product>()
                .ForMember(d => d.ThumbImage, o => o.Ignore())
                .ForMember(d => d.ProductContents, o => o.Ignore())
                .ForMember(d => d.ProductInventories, o => o.Ignore())
                .ForMember(d => d.ProductStockMasters, o => o.Ignore())
                .ForMember(d => d.PromotionalPrices, o => o.Ignore())
                .ForMember(d => d.Testimonials, o => o.Ignore());

            CreateMap<ProductInventory, ProductInventoryDto>()
               .ForMember(d => d.BrandName, o => o.MapFrom(s => s.BrandId > 0 ? s.Brand.Name : ""))
               .ForMember(d => d.ProductName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.Name : ""))
               .ForMember(d => d.UoMText, o => o.MapFrom(s => s.UoM > 0 ? s.Product.UoM1.ShortName : ""))
               .ForMember(d => d.EntryDateText, o => o.MapFrom(s => s.EntryDate != null ? ((DateTime)s.EntryDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""))
               .ForMember(d => d.ExpiryDateText, o => o.MapFrom(s => s.ExpirayDate != null ? ((DateTime)s.ExpirayDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ProductInventoryDto, ProductInventory>()
                 .ForMember(d => d.EntryDate, o => o.Ignore());

            CreateMap<ProductStockMaster, ProductStockMasterDto>()
               .ForMember(d => d.ProductName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.Name : ""))
               .ForMember(d => d.LastUpdatedText, o => o.MapFrom(s => s.LastUpdated != null ? ((DateTime)s.LastUpdated).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ProductStockMasterDto, ProductStockMaster>()
                .ForMember(d => d.ProductStockOutLogs, o => o.Ignore());

            CreateMap<ProductStockOutLog, ProductStockOutLogDto>()
               .ForMember(d => d.ProductName, o => o.MapFrom(s => s.MasterId > 0 ? s.ProductStockMaster.Product.Name: ""))
               .ForMember(d => d.EffectDateText, o => o.MapFrom(s => s.EffectDate != null ? ((DateTime)s.EffectDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ProductStockOutLogDto, ProductStockOutLog>();

            

            CreateMap<SEO, SEODto>()
               .ForMember(d => d.LanguageName, o => o.MapFrom(s => s.LangId > 0 ? s.Language.LanguageName : ""))
               .ForMember(d => d.PageTypeName, o => o.MapFrom(s => s.PageType > 0 ? UiUtil.GetDisplayName((PageType)s.PageType) : ""))
               .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdate != null ? ((DateTime)s.LastUpdate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<SEODto, SEO>();

            CreateMap<HtmlDto, SEODto>()
                .ForMember(d => d.SEOTitle, o => o.MapFrom(s => s.Title != "" ? s.Title : "Blank"))
                .ForMember(d => d.SEODescription, o => o.MapFrom(s => s.SubTitle != "" ? s.SubTitle : ""))
                .ForMember(d => d.Keyword, o => o.MapFrom(s => s.Title != "" ? s.Title : ""))
                .ForMember(d => d.Robot, o => o.MapFrom(s => s.Title != "" ? true : false))
                .ForMember(d => d.PageType, o => o.MapFrom(s=>s.Id>0 ? PageType.Html : PageType.Html))
                .ForMember(d => d.PageId, o => o.MapFrom(s =>  s.Id))
                .ForMember(d => d.LangId, o => o.MapFrom(s => s.LangId));

            CreateMap<BlogDto, SEODto>()
               .ForMember(d => d.SEOTitle, o => o.MapFrom(s => s.Title != "" ? s.Title : "Blank"))
               .ForMember(d => d.SEODescription, o => o.MapFrom(s => s.SmallDetails != "" ? s.SmallDetails : ""))
               .ForMember(d => d.Keyword, o => o.MapFrom(s => s.Title != "" ? s.Title : ""))
               .ForMember(d => d.Robot, o => o.MapFrom(s => s.Title != "" ? true : false))
               .ForMember(d => d.PageType, o => o.MapFrom(s => s.Id > 0 ? PageType.List : PageType.List))
               .ForMember(d => d.PageId, o => o.MapFrom(s => s.Id))
               .ForMember(d => d.LangId, o => o.MapFrom(s => s.LangId));

            
            CreateMap<Promotion, PromotionDto>()
                .ForMember(d => d.Image, d => d.Ignore())
                .ForMember(d => d.ImagePath, o => o.MapFrom(s => s.Image != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Image.Replace("\\", "/")) : ""))
                .ForMember(d => d.TypeName, o => o.MapFrom(s => s.PromotionType > 0 ? UiUtil.GetDisplayName((PromotionType)s.PromotionType) : ""))
               .ForMember(d => d.StartDateText, o => o.MapFrom(s => s.StartDate != null ? ((DateTime)s.StartDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""))
               .ForMember(d => d.EndDateText, o => o.MapFrom(s => s.EndDate != null ? ((DateTime)s.EndDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""))
               .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
               .ForMember(d => d.LastUpdatedText, o => o.MapFrom(s => s.LastUpdated != null ? ((DateTime)s.LastUpdated).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<PromotionDto, Promotion>()
                .ForMember(d => d.Image, d => d.Ignore())
                .ForMember(d => d.PromotionalPrices, o => o.Ignore());

            CreateMap<PromotionalPrice, PromotionalPriceDto>()
                .ForMember(d => d.ProductName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.Name : ""))
                .ForMember(d => d.CategoryId, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.CategoryId : 0))
                .ForMember(d => d.CategoryName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.ProductCategory.CategoryName : ""))
                .ForMember(d => d.PromotionTitle, o => o.MapFrom(s => s.PromotionId > 0 ? s.Promotion.PromotionTitle : ""));
            CreateMap<PromotionalPriceDto, PromotionalPrice>();

            CreateMap<ShoppingCart, ShoppingCartDto>()
               .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((CartStatus)s.Status) : "Main Cart"))
               .ForMember(d => d.CustomerName, o => o.MapFrom(s => s.CustomerId > 0 ? (s.Customer.FirstName + " " + s.Customer.LastName): ""))
               .ForMember(d => d.LastUpdateText, o => o.MapFrom(s => s.LastUpdated != null ? ((DateTime)s.LastUpdated).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<ShoppingCartDto, ShoppingCart>();

            CreateMap<ShoppingCartDetail, ShoppingCartDetailDto>()
               .ForMember(d => d.ProductName, o => o.MapFrom(s => s.ProductId > 0 ? s.Product.Name : ""));
            CreateMap<ShoppingCartDetailDto, ShoppingCartDetail>();

            CreateMap<Team, TeamDto>()
                .ForMember(d => d.Photo, d => d.Ignore())
                .ForMember(d => d.PhotoPath, o => o.MapFrom(s => s.Photo != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Photo.Replace("\\", "/")) : ""))
                .ForMember(d => d.BranchName, o => o.MapFrom(s => s.BranchId > 0 ? s.Branch.BranchName : ""))
                .ForMember(d => d.StatusText, o => o.MapFrom(s => s.Status > 0 ? UiUtil.GetDisplayName((EntityStatus)s.Status) : "Deleted"))
                .ForMember(d => d.LanguageName, o => o.MapFrom(s => s.LangId > 0 ? s.Language.LanguageName : ""))
                .ForMember(d => d.LastÚpdatedText, o => o.MapFrom(s => s.LastUpdated != null ? ((DateTime)s.LastUpdated).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<TeamDto, Team>()
                .ForMember(d => d.Photo, o => o.Ignore());

            CreateMap<Tag, TagDto>()
                .ForMember(d => d.TypeName, o => o.MapFrom(s => s.Type > 0 ? UiUtil.GetDisplayName((TagType)s.Type) : "Blog"));
            CreateMap<TagDto, Tag>();

            CreateMap<Testimonial, TestimonialDto>()
                .ForMember(d => d.Photo, d => d.Ignore())
                .ForMember(d => d.PhotoPath, o => o.MapFrom(s => s.Photo != null ? ("https://" + System.Web.HttpContext.Current.Request.Url.Authority + "/" + s.Photo.Replace("\\", "/")) : ""))
                .ForMember(d => d.CommentDateText, o => o.MapFrom(s => s.CommentDate != null ? ((DateTime)s.CommentDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<TestimonialDto, Testimonial>()
                .ForMember(d => d.Photo, d => d.Ignore());

            CreateMap<UoM, UoMDto>();
            CreateMap<UoMDto, UoM>()
                .ForMember(d => d.Products, o => o.Ignore());

        }
    }
}