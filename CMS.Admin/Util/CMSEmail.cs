﻿using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace CMS.Admin.Util
{
   public static class CMSEmail
    {
        public static ResponseDto Send(EmailDto dto)
        {
            ResponseDto response = new ResponseDto();

            try
            {
                MailMessage msg = new MailMessage();

                msg.To.Add(dto.ToAddress);

                MailAddress address = new MailAddress("donot-reply@parlasushi.se");
                msg.From = address;
                msg.Subject = dto.Subject;
                msg.Body = dto.EmailBody;
                msg.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("parlasushi.se", 587);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;


                //Setup credentials to login to our sender email address ("UserName", "Password")
                client.UseDefaultCredentials = false;
                NetworkCredential credentials = new NetworkCredential("donot-reply@parlasushi.se", "ntN8q8?7");
                client.Credentials = credentials;

                //Send the msg
                client.Send(msg);

                response.Success = true;
                response.Message = "Thank you for Your request. We will get back to you soon.";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Something wrong! We will get back to you soon.";
            }
            return response;
        }
    }
}
