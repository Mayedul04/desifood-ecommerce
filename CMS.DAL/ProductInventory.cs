//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMS.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductInventory
    {
        public long Id { get; set; }
        public Nullable<long> ProductId { get; set; }
        public Nullable<double> BasePrice { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public Nullable<double> Quantity { get; set; }
        public Nullable<long> UoM { get; set; }
        public Nullable<System.DateTime> ExpirayDate { get; set; }
        public Nullable<long> BrandId { get; set; }
    
        public virtual Brand Brand { get; set; }
        public virtual Product Product { get; set; }
    }
}
