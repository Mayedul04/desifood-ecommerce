$.noConflict();
(function ($) {
  'use strict';

  /*-------------------------------------------------------------------------------
  Cookies
  -------------------------------------------------------------------------------*/
  function setCookie(cname, cvalue, days) {

    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      var expires = "; expires=" + date.toGMTString();
    } else {
      var expires = "";
    }
    document.cookie = cname + "=" + cvalue + expires + "; path=/";
  }

  //Return a particular cookie
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  //Checks if a cookie exists
  function checkCookie(cookieToCheck) {
    var cookie = getCookie(cookieToCheck);
    if (cookie != "") {
      return true;
    }
    return false;
  }

  //Delet an existing cookie
  function deleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  /*-------------------------------------------------------------------------------
  Newsletter popup close and set cookie
  -------------------------------------------------------------------------------*/
  $(".newsletter-popup-trigger").on('click', function () {
    setCookie('newsletter_popup_viewed', 'true');
  });

  $('#androNewsletterPopup').on('hidden.bs.modal', function () {
    setCookie('newsletter_popup_viewed', 'true');
  });



  /*-------------------------------------------------------------------------------
  Preloader
	-------------------------------------------------------------------------------*/
  $(window).on('load', function () {
    $('.andro_preloader').addClass('hidden');

    if (!checkCookie('newsletter_popup_viewed')) {
      setTimeout(function () {
        $("#androNewsletterPopup").modal('show');
      }, 3000);
    }

  });

  

  /*-------------------------------------------------------------------------------
  Custom scroll bars
  -------------------------------------------------------------------------------*/
  // $('.andro_dropdown-scroll').slimScroll({
  //   height: 300,
  //   position: "right",
  //   size: "5px",
  //   color: "#dcdcdc",
  //   opacity: 1,
  //   wheelStep: 5,
  //   touchScrollStep: 50,
  // });

  /*-------------------------------------------------------------------------------
  Sticky Header
	-------------------------------------------------------------------------------*/
  var header = $(".can-sticky");
  var headerHeight = header.innerHeight();

  function doSticky() {
    if (window.pageYOffset > headerHeight) {
      header.addClass("sticky");
    } else {
      header.removeClass("sticky");
    }
  }
  doSticky();

  /*-------------------------------------------------------------------------------
  Tooltips
  -------------------------------------------------------------------------------*/
  $('[data-toggle="tooltip"]').tooltip();

  /*-------------------------------------------------------------------------------
  Magnific Popup
  -------------------------------------------------------------------------------*/
  //$('.popup-youtube').magnificPopup({
  //  type: 'iframe'
  //});
  //$('.popup-vimeo').magnificPopup({
  //  type: 'iframe'
  //});
  //$('.popup-video').magnificPopup({
  //  type: 'iframe'
  //});
  //$('.gallery-thumb').magnificPopup({
  //  type: 'image',
  //  gallery: {
  //    enabled: true
  //  },
  //});

  /*-------------------------------------------------------------------------------
  ion Range Sliders (Price filter)
  -------------------------------------------------------------------------------*/
  //$(".js-range-slider").ionRangeSlider();

  //$('.andro_product-single-thumb')
  //  .wrap('<span style="display:inline-block" class="andro_product-single-zoom"></span>')
  //  .css('display', 'block')
  //  .parent()
  //  .zoom();

  /*-------------------------------------------------------------------------------
  Countdown
  -------------------------------------------------------------------------------*/
  //$(".andro_countdown-timer").each(function(){
  //  var $this = $(this);
  //  $this.countdown($this.data('countdown'), function(event) {
  //    $(this).text(
  //      event.strftime('%D days %H:%M:%S')
  //    );
  //  });
  //});
  //$('time').countDown();
  /*-------------------------------------------------------------------------------
  
  

  

  /*-------------------------------------------------------------------------------
  Add / Subtract Quantity
  -------------------------------------------------------------------------------*/
  $(document).ready(function () {


  /*-------------------------------------------------------------------------------
Aside Menu
-------------------------------------------------------------------------------*/
  $(".aside-trigger-right").on('click', function() {
    var $el = $(".andro_aside-right")
    $el.toggleClass('open');
    if ($el.hasClass('open')) {
      setTimeout(function(){
        $el.find('.sidebar').fadeIn();
      }, 300);
    }else{
      $el.find('.sidebar').fadeOut();
    }
  });

  $(".aside-trigger-left").on('click', function() {
    $(".andro_aside-left").toggleClass('open');
  });

  $(".andro_aside .menu-item-has-children > a").on('click', function(e) {
    var submenu = $(this).next(".sub-menu");
    e.preventDefault();

    submenu.slideToggle(200);
  });

    /*-------------------------------------------------------------------------------
  Countdown
  -------------------------------------------------------------------------------*/
    //$('time').countDown();
    //$(".andro_countdown-timer").each(function () {
    //  var $this = $(this);
    //  $this.countdown($this.data('countdown'), function (event) {
    //    $(this).text(
    //      event.strftime('%D days %H:%M:%S')
    //    );
    //  });
    //});

    $(".qty span").on('click', function () {
      alert('working!');
      var qty = $(this).closest('.qty').find('input');
      var qtyVal = parseInt(qty.val());
      if ($(this).hasClass('qty-add')) {
        qty.val(qtyVal + 1);
      } else {
        return qtyVal > 1 ? qty.val(qtyVal - 1) : 0;
      }
    });

    /*-------------------------------------------------------------------------------
  Masonry
  -------------------------------------------------------------------------------*/
    //$('.masonry').imagesLoaded(function () {
    //  var isotopeContainer = $('.masonry');
    //  isotopeContainer.isotope({
    //    itemSelector: '.masonry-item',
    //  });
    //});
    /*-----------------------------------
    Back to Top
    -----------------------------------*/
    $('.andro_back-to-top').on('click', function () {
      $("html, body").animate({
        scrollTop: 0
      }, 600);
      return false;
    })
  });




  //On scroll events
  $(window).on('scroll', function () {

    doSticky();

  });

  //On resize events
  $(window).on('resize', function () {


  });

})(jQuery);
