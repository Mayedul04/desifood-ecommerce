﻿$(document).ready(function () {
    var promoid = getParameterByName(window.location.href, "promoid");
    function populateCategory(isEdit) {
        var ddlcategory = $("#tr_CategoryId select");
        var ddlproducts = $("#tr_ProductId select");
        $(ddlcategory).attr("id", "CategoryId").attr("name", "CategoryId");
        $(ddlproducts).attr("id", "ProductId").attr("name", "ProductId");

        var selectedcategoryId = $("#jqGrid").jqGrid('getRowData', $("#jqGrid")[0].p.selrow).CategoryId | 0;
        $(ddlcategory)
            .html("<option value=''>Loading Category...</option>")
            .attr("disabled", "disabled");
        $.ajax({
            url: '/Admin/Product/GetCategoriesJQ',
            type: "GET",
            success: function (officelayerHtml) {
                $(ddlcategory).removeAttr("disabled").html(officelayerHtml);

                if (isEdit) {
                    $(ddlcategory).val(selectedcategoryId);
                } else {
                    $(ddlcategory).selectedIndex = 0;
                }
                updateCategoryCallBack(isEdit, $(ddlcategory).val(), ddlproducts);
            }
        });
        $(ddlcategory).bind("change", function (e) {
            updateCategoryCallBack(false, $(ddlcategory).val(), ddlproducts);
        });
    }

    function updateCategoryCallBack(isEdit, selectedcategoryId, ddlproducts) {
        var url = '/Admin/Product/GetAllProducts?catid=' + selectedcategoryId;
        var price = $("#tr_BasePrice input");
        $(ddlproducts)
            .html("<option value=''>Loading products...</option>")
            .attr("disabled", "disabled");
        $.ajax({
            url: url,
            type: "GET",
            success: function (officeJson) {
                var products = eval(officeJson);
                var officeHtml = "";
                $(products).each(function (i, option) {
                    officeHtml += '<option value="' + option.Id + '">' + option.Name + '</option>';
                });
                $(ddlproducts).removeAttr("disabled").html(officeHtml);
                if (isEdit) {
                    var selectedProductId = $("#jqGrid").jqGrid('getRowData', $("#jqGrid")[0].p.selrow).ProductId | 0;
                    $(ddlproducts).val(selectedProductId);
                } else {
                    $(ddlproducts).selectedIndex = 0;
                }
                updateProductCallBack(true, $(ddlproducts).val(), price);
            }
        });
        $(ddlproducts).bind("change", function (e) {
            updateProductCallBack(false, $(ddlproducts).val(), price);
        });
    }
    function updateProductCallBack(isEdit, prodid, price) {
        $.ajax({
            url: '/Admin/Product/GetProductBasePriceJQ?prodid=' + prodid,
            type: "GET",
            success: function (officelayerHtml) {
                $(price).val(officelayerHtml);
            }
        });
    }
    function getParameterByName(url, name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    $(function () {
        $("#jqGrid").jqGrid({
            url: "/Admin/Promotions/GetPromoPrices?promoid=" + promoid,
            datatype: 'json',
            mtype: 'Get',
            colNames: ['Id', 'PromoId' ,'Promo Name', 'CategoryId', 'Category Name', 'ProductId','Product Name', 'Base Price', 'Discount', 'Flat Amount', 'Latest Price'],
            colModel: [
                { key: true, hidden: true, name: 'Id', index: 'Id', editable: false },
                { key: false, hidden: true, name: 'PromotionId', index: 'PromotionId', label: 'Promo Id', editable: true, editoptions: { defaultValue: promoid } },
                { key: false, name: 'PromotionTitle', index: 'PromotionTitle', label: 'Promo Name', editable: false },
                { key: false, hidden: true, name: 'CategoryId', index: 'CategoryId', editable: true, edittype: "select", editrules: { required: true, edithidden: true }, formoptions: { label: "Category " }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'CategoryName', index: 'CategoryName', label: 'Category Name', editable: false },
                { key: false, hidden: true, name: 'ProductId', index: 'ProductId', editable: true, edittype: "select", editrules: { edithidden: true, required: true }, formoptions: { label: "Product " } },
                { key: false, name: 'ProductName', index: 'ProductName', label: 'Product Name', editable: false },
                { key: false, name: 'BasePrice', label: 'BasePrice', index: 'BasePrice', width: 140, editable: true, editrules: { custom_func: validatePositive, custom: true, required: true }, editoptions: { readonly: "readonly" }, align: 'right', formoptions: { label: "Base Price" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'Discount', label: 'Discount', index: 'Discount', width: 140, editable: true, editrules: { custom_func: validatePositive, custom: true }, align: 'right', formoptions: { label: "Discount" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'FlatAmount', label: 'FlatAmount', index: 'FlatAmount', width: 140, editable: true, editrules: { custom_func: validatePositive, custom: true}, align: 'right', formoptions: { label: "Flat Amount" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'LatestPrice', label: 'Latest Price', index: 'LatestPrice', width: 140, editable: false, align: 'right', formoptions: { label: "Latest Price" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                //{
                //    key: false, name: 'Amount', index: 'Amount', editable: true, edittype: 'text',
                //    editoptions: {
                //        dataInit: function (element) {
                //            $(element).keypress(function (e) {
                //                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //                    return false;
                //                }

                //            });
                //        }
                //    },
                //    editrules: { required: true }
                //},
                
            ],
            ondblClickRow: function (rowid) {
                jQuery("#jqGrid").jqGrid('editGridRow', rowid);
            },
            pager: jQuery('#jqControls'),
            rowNum: 10,
            rowList: [10, 20, 30, 40, 50],
            hoverrows: true,
            sortable: true,
            width: '50%',
            viewrecords: true,
            caption: 'Promotion Prices',
            emptyrecords: 'No Records are Available to Display',
            jsonReader: {
                root: "rows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems: false,
                Id: "0"
            },
            autowidth: true,
            height: 'auto',//set auto height
            multiselect: false
        }).navGrid('#jqControls',
        { edit: true, add: true, del: true, search: true, refresh: true },
        {
            zIndex: 100,
            url: '/Admin/Promotions/SavePrice',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            width: '400',
            height: 'auto',
            onInitializeForm: function (formId) { populateCategory(true);},
            afterComplete: function (response) {
                if (response.responseText) {
                    toastr.success(response.Message);
                }
            }
        },
        {
            zIndex: 100,
            url: "/Admin/Promotions/SavePrice",
            closeOnEscape: true,
            width: '400',
            height: 'auto',
            closeAfterAdd: true,
            onInitializeForm: function (formId) { populateCategory(true); },
            afterComplete: function (response) {
                //Messager.ShowMessage(response.Message);
                toastr.success(response.Message);
            }
        },
        {
            zIndex: 100,
            url: "/Admin/Promotions/RemovePrice",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure to delete this price? ",
            afterComplete: function (response) {
                Messager.ShowMessage(response.responseText);
            }
        },

        {
            closeOnEscape: true, multipleSearch: true,
            closeAfterSearch: true
        }
        );
    });
});