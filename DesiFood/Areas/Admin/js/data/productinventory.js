﻿$(document).ready(function () {
    var catid = getParameterByName(window.location.href, "catid");
    //function populateProducts(isEdit) {
    //    var ddlproducts = $("#tr_ProductId select");
    //    $(ddlproducts).attr("id", "ProductId").attr("name", "ProductId");
        
    //    var selectedProdId = $("#jqGrid").jqGrid('getRowData', $("#jqGrid")[0].p.selrow).ProductId | 0;
    //    var catid = getParameterByName(window.location.href, "catid");
        
    //    $(ddlproducts)
    //        .html("<option value=''>Loading Products...</option>")
    //        .attr("disabled", "disabled");
    //    $.ajax({
    //        url: '/Admin/Product/GetAllProductsJQ?catid=' + catid,
    //        type: "GET",
    //        success: function (brandlayerHtml) {
    //            $(ddlproducts).removeAttr("disabled").html(brandlayerHtml);

    //            if (isEdit) {
    //                $(ddlproducts).val(selectedProdId);
    //            } else {
    //                $(ddlproducts).selectedIndex = 0;
    //            }
    //        }
    //    });
    //    $(ddlproducts).bind("change", function (e) {
    //        //updateOfficeCallBack(false, $(officeLayerCombo).val(), officeCombo);
    //    });
    //}



    //function updateOfficeCallBack(isEdit, selectedOfficeLayerId, officeCombo) {
    //    var url = '/HRM/Office/GetOfficeByLayer/?officelayerid=' + selectedOfficeLayerId;
    //    $(officeCombo)
    //        .html("<option value=''>Loading offices...</option>")
    //        .attr("disabled", "disabled");
    //    $.ajax({
    //        url: url,
    //        type: "GET",
    //        success: function (officeJson) {
    //            var offices = eval(officeJson);
    //            var officeHtml = "";
    //            $(offices).each(function (i, option) {
    //                officeHtml += '<option value="' + option.Id + '">' + option.Name + '</option>';
    //            });
    //            $(officeCombo).removeAttr("disabled").html(officeHtml);
    //            if (isEdit) {
    //                var selectedOfficeId = $("#jqGrid").jqGrid('getRowData', $("#jqGrid")[0].p.selrow).OfficeId | 0;
    //                $(officeCombo).val(selectedOfficeId);
    //            } else {
    //                $(officeCombo).selectedIndex = 0;
    //            }
    //            $(officeCombo).focus();
    //        }
    //    });
    //}

    function getParameterByName(url, name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    $(function () {
        $("#jqGrid").jqGrid({
            url: "/Admin/Product/GetInventoryStock?catid="+catid,
            datatype: 'json',
            mtype: 'Get',
            colNames: ['Id', 'ProductId', 'Product Name', 'BrandId', 'BrandName', 'EntryDate', 'Entry Date', 'Qty', 'BasePrice', 'UoM', 'UoM Text', 'Expiry Date', 'Expiry Date Text' ],
            colModel: [
                { key: true, hidden: true, name: 'Id', index: 'Id', editable: false },
                {
                    key: false, hidden: true, name: 'ProductId', width: 140, index: 'ProductId', editable: true, edittype: "select", editoptions: {
                        dataUrl: '/Admin/Product/GetAllProductsJQ?catid=' + catid, cacheUrlData: true }, editrules: { edithidden: true, required: true },
                    formoptions: { label: "ProductId" }
                },
                { key: false, name: 'ProductName', index: 'ProductName', label: "Product Name", editable: false, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                {
                    key: false, hidden: true, name: 'BrandId', width: 140, index: 'BrandId', editable: true, edittype: "select", editoptions: { dataUrl: '/Admin/Brands/GetAllSuppliers', cacheUrlData: true }, editrules: { edithidden: true, required: true },
                    formoptions: { label: "Brand" }
                },
                { key: false, name: 'BrandName', index: 'BrandName', label: "BrandName", editable: false, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, hidden: true, name: 'EntryDate', label: 'EntryDate', index: 'EntryDate', width: 140, editable: true,  align: 'right', formoptions: { label: "Entry Date" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'EntryDateText', label: 'EntryDate', index: 'EntryDateText', width: 140, editable: false,  align: 'right', formoptions: { label: "Entry Date" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'Quantity', label: 'Quantity', index: 'Quantity', width: 140, editable: true, editrules: { custom_func: validatePositive, custom: true, required: true }, align: 'right', formoptions: { label: "Quantity" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'BasePrice', label: 'BasePrice', index: 'BasePrice', width: 140, editable: true, editrules: { custom_func: validatePositive, custom: true, required: true }, align: 'right', formoptions: { label: "Base Price" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, hidden: true, name: 'UoM', label: 'UoM', index: 'UoM', width: 140, editable: true, edittype: "select", editrules: { required: true }, align: 'right', editoptions: { dataUrl: '/Admin/Settings/UoMsJQ', cacheUrlData: true }, editrules: { edithidden: true, required: true }, formoptions: { label: "UoM" }, searchoptions: { sopt: ['eq', 'ne', 'cn'] }, classes: "grid-col" },
                { key: false, name: 'UoMText', label: 'UoMText', index: 'UoMText', width: 140, editable: false, align: 'right', formoptions: { label: "UoM Text" }, classes: "grid-col" },
                {
                    key: false,
                    name: "ExpirayDate", index: 'ExpirayDate', label: "ExpirayDate", formatter: "date", formatoptions: { srcformat: "ISO8601Long", newformat: "d/m/Y" }, editable: false, edittype: "text",
                    editoptions: {
                        dataInit: function (element) {
                            $(element).datepicker({
                                id: 'ExpirayDate_datePicker',
                                dateFormat: 'dd/MM/yyyy',
                                changeYear: true,
                                showOn: 'focus'
                            });
                        }
                    }
                },
                {
                    key: false,
                    name: "ExpiryDateText", hidden: true, index: 'ExpiryDateText', label: "Expiry Date", formatter: "text", formatoptions: { srcformat: "ISO8601Long", newformat: "d/m/Y" }, editable: true, editrules: { edithidden: true, required: true }, edittype: "text", formoptions: { label: "Expiry Date" },
                    editoptions: {
                        dataInit: function (element) {
                            $(element).datepicker({
                                id: 'ExpirayDate_datePicker',
                                dateFormat: 'dd/mm/yy',
                                changeYear: true,
                                showOn: 'focus'
                            });
                        }
                    }
                },
                
            ],
            ondblClickRow: function (rowid) {
                jQuery("#jqGrid").jqGrid('editGridRow', rowid);
            },
            loadonce: true,
            pager: jQuery('#jqControls'),
            rowNum: 10,
            rowList: [10, 20, 30, 40, 50],
            hoverrows: true,
            sortable: true,
            width: '70%',
            viewrecords: true,
            caption: 'Inventory Records',
            emptyrecords: 'No  Records are Available to Display',
            jsonReader: {
                root: "rows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems: false,
                Id: "0"
            },
            autowidth: true,
            height: 'auto',//set auto height
            multiselect: false
        }).navGrid('#jqControls',
        { edit: true, add: true, del:false, search: true, refresh: true },
        {

            zIndex: 100,
            url: '/Admin/Product/SaveInventory',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            width: '400',
            height: 'auto',
            recreateForm: true,
            //onInitializeForm: function (formId) { /*populateProducts(true);*/ },
            afterComplete: function (response) {
                if (response.responseText) {
                    toastr.success("Inventory has been Edited Successfully!");
                }
            }
        },
        {
            zIndex: 100,
            url: '/Admin/Product/SaveInventory',
            closeOnEscape: true,
            width: '400',
            height: 'auto',
            closeAfterAdd: true,
            //onInitializeForm: function (formId) { /*populateProducts(true);*/ },
            afterComplete: function (response) {
                //Messager.ShowMessage(response.Message);
                toastr.success("Inventory has been Saved Successfully!");
            }
        },

        {
            closeOnEscape: true, multipleSearch: true,
            closeAfterSearch: true
        }
        );
    });
});