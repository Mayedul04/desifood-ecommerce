﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class ContactUsController : Controller
    {
        // GET: Admin/ContactUs
        ContactFacade _contact = new ContactFacade();
     
        public ActionResult Index()
        {
            var data = _contact.GetAllContactMessages();
            return View(data);
        }
        [HttpPost]
        public JsonResult SaveMessage(ContactRequestDto dto)
        {
            dto.Action = CMS.Infrastructure.EmailAction.Received;
            var data = _contact.SaveContactMessage(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveMessage(long subid)
        {
            var data = _contact.DeleteMessage(subid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long subid)
        {
            var data = _contact.LoadMessage((long)subid);
            return View(data);
        }
    }
}