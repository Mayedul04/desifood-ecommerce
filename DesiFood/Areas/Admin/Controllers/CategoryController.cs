﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        ProductFacade _prod = new ProductFacade();
        BranchFacade _brance = new BranchFacade();
        MiscFacade _misc = new MiscFacade();
        // GET: Admin/Category
        public ActionResult Index()
        {
            var data = _prod.GetAllCategory();
            return View(data);
        }

        public ActionResult AddNew(long? cid, int? level = 0)
        {
            var categories = _prod.GetAllCategory().Where(x => (int)x.CatLevel == level).ToList();
            categories.Insert(0, null);

            ViewBag.Parents = categories;
            if (cid != null)
            {
                var data = _prod.LoadCategory((long)cid);
                return View(data);
            }
            else
                return View();
        }

        public ActionResult Details(long cid)
        {
            var data = _prod.LoadCategory((long)cid);
            return View(data);
        }

        [HttpPost]
        public JsonResult SaveCategory(ProductCategoryDto dto)
        {
            var data = _prod.SaveCategory(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveCategory(long cid)
        {
            var data = _prod.DeleteCategory(cid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }




        public ActionResult AddNewContent(long? cid, long? cdid)
        {
            if (cdid != null)
            {
                var data = _prod.LoadCategoryContentDetails((long)cdid);
                return View(data);
            }
            else
            {
                ViewBag.CategoryId = cid;
                ViewBag.CategoryName = _prod.LoadCategory((long)cid).CategoryName;
                return View();
            }

        }

        public ActionResult CategoryDetails(long cdid)
        {
            var data = _prod.LoadCategoryContentDetails((long)cdid);
            return View(data);
        }

        [HttpPost]
        public JsonResult SaveContent(CategoryDetailDto dto)
        {
            var data = _prod.SaveCategoryContent(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}