﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class TeamController : Controller
    {
        // GET: Admin/Team
        BranchFacade _branch = new BranchFacade();
        MiscFacade _misc = new MiscFacade();
        public ActionResult Index()
        {
            var data = _branch.GetAllTeamMembers(0);
            return View(data);
        }

        public ActionResult AddNew(long? memid)
        {
            var languages = _misc.GetAllLanguages();
            ViewBag.Languages = from s in languages
                                select new { Id = s.Id, LanguageName = s.ShortName };
            var branches = _branch.GetAllLists();
            ViewBag.Branches = from s in branches
                                select new { Id = s.Id, BranchName = s.BranchName };
            if (memid != null)
            {
                var data = _branch.LoadProfile((long)memid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveMemberInfo(TeamDto dto)
        {
            var data = _branch.SaveTeamMember(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveList(long memid)
        {
            var data = _branch.RemoveTeamMember(memid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long memid)
        {
            var data = _branch.LoadProfile((long)memid);
            return View(data);
        }
    }
}