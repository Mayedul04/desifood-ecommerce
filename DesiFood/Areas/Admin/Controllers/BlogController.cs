﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class BlogController : Controller
    {
        // GET: Admin/Blog
        BlogFacade _list = new BlogFacade();
        MiscFacade _misc = new MiscFacade();
        GalleryFacade _gallery = new GalleryFacade();
       
        public ActionResult Index()
        {
            var data = _list.GetAllLists();
            return View(data);
        }
        public ActionResult AddNew(long? lid)
        {
            var languages = _misc.GetAllLanguages();
            ViewBag.Languages = from s in languages
                                select new { Id = s.Id, LanguageName = s.ShortName };
            if (lid != null)
            {
                var data = _list.LoadList((long)lid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveList(BlogDto dto)
        {
            var data = _list.SaveList(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveList(long lid)
        {
            var data = _list.DeleteList(lid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long lid)
        {
            var data = _list.LoadList((long)lid);
            return View(data);
        }

        #region Blog Contents
        public ActionResult AddNewContent(long? lid, long? contid)
        {
            var galleries = _gallery.GetAllGallery().ToList();
            galleries.Insert(0, null);

            var contentypes = _misc.GetAllContentTypes("").ToList();
            contentypes.Insert(0, null);

            ViewBag.Galleries = galleries;
            ViewBag.ContentTypes = contentypes;
            if (contid != null)
            {
                var data = _list.LoadContent((long)contid);
                return View(data);
            }
            else
            {
                ViewBag.BlogId = lid;
                ViewBag.BlogTitle = _list.LoadList((long)lid).Title;
                return View();
            }
        }
        [HttpPost]
        public JsonResult SaveContent(BlogContentDto dto)
        {
            var data = _list.SaveContent(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContentDetails(long contid)
        {
            var data = _list.LoadContent((long)contid);
            return View(data);
        }
        [HttpPost]
        public JsonResult RemoveContent(long contid)
        {
            var data = _list.DeleteContent(contid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Blog Comments
        //Coming Soon
        #endregion
    }
}