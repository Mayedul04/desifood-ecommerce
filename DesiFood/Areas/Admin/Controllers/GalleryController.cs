﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class GalleryController : Controller
    {
        GalleryFacade _gallery = new GalleryFacade();
        // GET: Admin/Gallery
        public ActionResult Index()
        {
            var data = _gallery.GetAllGallery();
            return View(data);
        }
        public ActionResult AddNew(long? gid, int? level=0)
        {
            var galleries = _gallery.GetAllGallery().Where(x=>x.GalleryLevel== level).ToList();
            galleries.Insert(0, null);
           
            ViewBag.Parents = galleries;
            if (gid != null)
            {
                var data = _gallery.LoadGallery((long)gid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveGallery(GalleryDto dto)
        {
            var data = _gallery.SaveGallery(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveGallery(long gid)
        {
            var data = _gallery.DeleteGallery(gid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long gid)
        {
            var data = _gallery.LoadGallery((long)gid);
            return View(data);
        }
        public ActionResult AddNewImage(long? gid, long? imageid)
        {
            if (imageid != null)
            {
                var data = _gallery.LoadImage((long)imageid);
                return View(data);
            }
            else
            {
                ViewBag.GalleryId = gid;
                ViewBag.GalleryName = _gallery.LoadGallery((long)gid).Title;
                return View();
            }

        }
        [HttpPost]
        public JsonResult SaveImage(GalleryItemDto dto)
        {
            var data = _gallery.SaveImage(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImageDetails(long imageid)
        {
            var data = _gallery.LoadImage((long)imageid);
            return View(data);
        }
        [HttpPost]
        public JsonResult RemoveImage(long imageid)
        {
            var data = _gallery.DeleteImage(imageid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}