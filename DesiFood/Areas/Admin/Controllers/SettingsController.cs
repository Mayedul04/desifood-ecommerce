﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class SettingsController : Controller
    {
        TagFacade _category = new TagFacade();
        MiscFacade _misc = new MiscFacade();
        // GET: Admin/Category
        #region Tags
        public ActionResult BlogTags()
        {
            var data = _category.GetAllTags(CMS.Infrastructure.TagType.Blog);
            return View(data);
        }

        public ActionResult MenuTags()
        {
            var data = _category.GetAllTags(CMS.Infrastructure.TagType.Menu);
            return View(data);
        }

        public ActionResult AddNew(long? tid)
        {
            if (tid != null)
            {
                var data = _category.LoadTag((long)tid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveTag(TagDto dto)
        {
            var data = _category.SaveTag(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveTag(long id)
        {
            var data = _category.DeleteTag(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long tid)
        {
            var data = _category.LoadTag((long)tid);
            return View(data);
        } 
        #endregion

        #region Contact Subjects
        public ActionResult ContactSubjects()
        {
            var data = _misc.GetAllContactSubjects();
            return View(data);
        }

        public ActionResult AddNewSubject(long? subid)
        {
            var languages = _misc.GetAllLanguages();

            ViewBag.Languages = from s in languages
                                select new { Id = s.Id, LanguageName = s.ShortName };
            if (subid != null)
            {
                var data = _misc.LoadContactSubject((long)subid);
                return View(data);
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult SaveSubject(ContactSubjectDto dto)
        {
            var data = _misc.SaveContactSubject(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveSubject(long subid)
        {
            var data = _misc.DeleteSubject(subid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SubjectDetails(long subid)
        {
            var data = _misc.LoadContactSubject((long)subid);
            return View(data);
        }
        #endregion

        #region Content Types
        public ActionResult ContactTypes()
        {
            var data = _misc.GetAllContentTypes("");
            return View(data);
        }

        public ActionResult AddContentType(long? contid)
        {
            if (contid != null)
            {
                var data = _misc.LoadContentType((long)contid);
                return View(data);
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult SaveContentType(ContentTypeDto dto)
        {
            var data = _misc.SaveContentType(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveContentType(long contid)
        {
            var data = _misc.DeleteContentType(contid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ContentTypeDetails(long contid)
        {
            var data = _misc.LoadContentType((long)contid);
            return View(data);
        }
        #endregion

        #region UoMs
        public ActionResult UoMs()
        {
            var data = _misc.GetAllUoMs();
            return View(data);
        }
        [HttpGet]
        public string UoMsJQ()
        {
            var data = _misc.GetAllUoMs();
            string strret = "<select>";
            foreach (UoMDto item in data)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        public ActionResult AddNewUoM(long? oid)
        {
            if (oid != null)
            {
                var data = _misc.LoadUoM((long)oid);
                return View(data);
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult SaveUoM(UoMDto dto)
        {
            var data = _misc.SaveUoM(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveUoM(long oid)
        {
            var data = _misc.DeleteUoM(oid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UoMDetails(long oid)
        {
            var data = _misc.LoadUoM((long)oid);
            return View(data);
        } 
        #endregion
    }
}