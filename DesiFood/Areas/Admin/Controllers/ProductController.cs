﻿
using CMS.Admin.Facade;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        ProductFacade _prod = new ProductFacade();
        BranchFacade _brance = new BranchFacade();
        MiscFacade _misc = new MiscFacade();
        GalleryFacade _gallery = new GalleryFacade();
        // GET: Admin/Product
        #region Product Master
        public ActionResult Index(int catid)
        {
            ProductCategoryDto catdetails = _prod.LoadCategory((long)catid);
            ViewBag.CatId = catid;
            ViewBag.CategoryName = catdetails.CategoryName;
            ViewBag.ParentName = catdetails.ParentCategory;
            ViewBag.ParentId = catdetails.ParentId;
            var data = _prod.GetAllChildProductByCategory(catid);
            return View(data);
        }
        public JsonResult GetCategories()
        {
            var data = _prod.GetAllCategory(1);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetCategoriesJQ()
        {
            var data = _prod.GetAllCategory(1);
            string strret = "<select>";
            foreach (ProductCategoryDto item in data)
            {
                strret += "<option value='" + item.Id + "'>" + item.CategoryName + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        public ActionResult AddNew(long catid, long? pid)
        {
            var galleries = _gallery.GetAllGallery().ToList();
            galleries.Insert(0, null);
            var uomlist = _misc.GetAllUoMs();
            uomlist.Insert(0, null);
            ViewBag.UoMs = uomlist;
            ViewBag.Galleries = galleries;
            var languages = _misc.GetAllLanguages();

            ViewBag.CatId = catid;
            ViewBag.CategoryName = _prod.LoadCategory((long)catid).CategoryName;
            ViewBag.Languages = from s in languages
                                select new { Id = s.Id, LanguageName = s.ShortName };
            if (pid != null)
            {
                var data = _prod.LoadProduct((long)pid);
                return View(data);
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public JsonResult SaveProduct(ProductDto dto)
        {
            var data = _prod.SaveProduct(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveProduct(long pid)
        {
            var data = _prod.DeleteProduct(pid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long pid)
        {
            var data = _prod.LoadProduct((long)pid);
            return View(data);
        }
        #endregion


        #region Product Contents
        public ActionResult AddNewContent(long? pid, long? pcid)
        {
            var galleries = _gallery.GetAllGallery().ToList();
            galleries.Insert(0, null);

            var contentypes = _misc.GetAllContentTypes("").ToList();
            contentypes.Insert(0, null);

            ViewBag.Galleries = galleries;
            ViewBag.ContentTypes = contentypes;
            if (pcid != null)
            {
                var data = _prod.LoadContent((long)pcid);
                return View(data);
            }
            else
            {
                ViewBag.ProductId = pid;
                ViewBag.ProductName = _prod.LoadProduct((long)pid).Name;
                return View();
            }
        }
        [HttpPost]
        public JsonResult SaveContent(ProductContentDto dto)
        {
            var data = _prod.SaveContent(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContentDetails(long pcid)
        {
            var data = _prod.LoadContent((long)pcid);
            return View(data);
        }
        [HttpPost]
        public JsonResult RemoveContent(long pcid)
        {
            var data = _prod.DeleteContent(pcid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult InventoryStock(long catid)
        {
            return View();
        }

        public JsonResult GetInventoryStock(long catid)
        {
            var data = _prod.GetAllInventory(catid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllProducts(long catid)
        {
            //var data = _prod.GetAllProducts();
            var data = _prod.GetAllChildProductByCategory(catid);
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public string GetAllProductsJQ(long catid)
        {
            var data = _prod.GetAllChildProductByCategory(catid);
            string strret = "<select>";
            foreach (ProductDto item in data)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public double GetProductBasePriceJQ(long prodid)
        {
            var data = _prod.getInventory(prodid);
            return (double)data.BasePrice;
        }

        [HttpPost]
        public JsonResult SaveInventory(ProductInventoryDto dto)
        {
            if (!string.IsNullOrEmpty(dto.ExpiryDateText))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(dto.ExpiryDateText, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    dto.ExpirayDate = changeDate;
            }
            var data = _prod.SaveInventory(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
 }