﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class CMSUserController : Controller
    {
        // GET: Admin/CMSUser
        AdminUsersFacade _user = new AdminUsersFacade();
        
        public ActionResult Index()
        {
            var data = _user.GetAllUsers();
            return View(data);
        }
        public ActionResult AddNew(long? userid)
        {
            if (userid != null)
            {
                var data = _user.LoadUserInfo((long)userid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveUser(AdminPanelLoginsDto dto)
        {
            var data = _user.SaveAdminUser(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveUser(long userid)
        {
            var data = _user.DeleteUser(userid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long userid)
        {
            var data = _user.LoadUserInfo((long)userid);
            return View(data);
        }
    }
}