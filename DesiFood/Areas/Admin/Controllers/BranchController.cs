﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class BranchController : Controller
    {
        // GET: Admin/Branch
        BranchFacade _branch = new BranchFacade();
        MiscFacade _misc = new MiscFacade();
        #region Branch
        public ActionResult Index()
        {
            var data = _branch.GetAllLists();
            return View(data);
        }
        public ActionResult AddNew(long? bid)
        {
            var languages = _misc.GetAllLanguages();
            ViewBag.Languages = from s in languages
                                select new { Id = s.Id, LanguageName = s.ShortName };
            if (bid != null)
            {
                var data = _branch.LoadList((long)bid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveBranch(BranchDto dto)
        {
            var data = _branch.SaveList(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveList(long lid)
        {
            var data = _branch.DeleteList(lid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long bid)
        {
            var data = _branch.LoadList((long)bid);
            return View(data);
        }
        #endregion


        #region Restaurant Opening Hours


        public ActionResult AddOpeningHour(long? brid, long? oid)
        {
            var languages = _misc.GetAllLanguages();
            ViewBag.Languages = from s in languages
                                select new { Id = s.Id, LanguageName = s.ShortName };
            if (oid != null)
            {
                var data = _branch.LoadOpeningHours((long)oid);
                return View(data);
            }
            else
            {
                ViewBag.BranchId = brid;
                ViewBag.BranchName = _branch.LoadList((long)brid).BranchName;
                return View();
            }
        }
        [HttpPost]
        public JsonResult SaveOpeningHour(OpeningHourDto dto)
        {
            var data = _branch.SaveOpeningHour(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OpeningHourDetails(long oid)
        {
            var data = _branch.LoadOpeningHours((long)oid);
            return View(data);
        }
        [HttpPost]
        public JsonResult RemoveOpeningHour(long oid)
        {
            var data = _branch.DeleteHours(oid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}