﻿using CMS.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            if (SessionHelper.GetCookie() > 0)
                return View();
            else
                return RedirectToAction("Index", "Login", new { area = "Admin" });
        }
    }
}