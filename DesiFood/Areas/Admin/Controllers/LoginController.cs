﻿using CMS.Dto;
using CMS.Admin.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.UI;

namespace DesiFood.Areas.Admin.Controllers
{
    
    public class LoginController : Controller
    {
        AdminUsersFacade _login = new AdminUsersFacade();

       

        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignIn(AdminPanelLoginsDto dto)
        {
            UserResourceDto vm = new UserResourceDto();
            vm = _login.DoLogin(dto);
            if (vm.UserId > 0)
            {
                SessionHelper.UserProfile = vm;
                SessionHelper.SetCookie(vm,7);
                SessionHelper.SetInSession("CMS_User",vm);
                
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            }
            else
            {
                ViewBag.Message = "Wrong Credentials";
                return RedirectToAction("Index", "Login", new { area = "Admin" });
            }
 
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            SessionHelper.DeleteCookie();
            return RedirectToAction("Index", "Login");
        }
    }
}