﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class PromotionsController : Controller
    {
        // GET: Admin/Promotions
        PromotionFacade _promo = new PromotionFacade();
        ProductFacade _prod = new ProductFacade();
        public ActionResult Index()
        {
            var data = _promo.GetAllPromotion();
            return View(data);
        }
        public ActionResult AddNew(long? promoid)
        {
            if (promoid != null)
            {
                var data = _promo.LoadPromotion((long)promoid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SavePromo(PromotionDto dto)
        {
            var data = _promo.SavePromotion(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemovePromotion(long promoid)
        {
            var data = _promo.DeleteList(promoid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long promoid)
        {
            var data = _promo.LoadPromotion((long)promoid);
            return View(data);
        }
        public ActionResult PromoPrices()
        {
            return View();
        }

        public JsonResult GetPromoPrices(long promoid)
        {
            var data = _promo.GetAllPromotionPrices(promoid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddNewPrice(long? promoid, long? pid)
        {
            var products = _prod.GetAllChildProductByCategory(2).ToList();
            products.Insert(0, null);
            ViewBag.Products = products;
            if (pid != null)
            {
                var data = _promo.LoadPrice((long)pid);
                return View(data);
            }
            else
            {
                ViewBag.PromotionId = promoid;
                ViewBag.PromotionTitle = _promo.LoadPromotion((long)promoid).PromotionTitle;
                return View();
            }
        }

        [HttpPost]
        public JsonResult SavePrice(PromotionalPriceDto dto)
        {
            
            var data = _promo.SavePrice(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemovePrice(PromotionalPriceDto dto)
        {
            var data = _promo.DeletePrice(dto.Id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}