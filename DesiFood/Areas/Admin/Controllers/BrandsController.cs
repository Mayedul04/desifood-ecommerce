﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class BrandsController : Controller
    {
        MiscFacade _misc = new MiscFacade();
        // GET: Admin/Brands
        public ActionResult Buyers()
        {
            var data = _misc.GetAllBrands((int)CMS.Infrastructure.BrandType.Buyers);
            return View(data);
        }

        public ActionResult Suppliers()
        {
            var data = _misc.GetAllBrands((int)CMS.Infrastructure.BrandType.Supplier);
            return View(data);
        }
        [HttpGet]
        public string GetAllSuppliers()
        {
            var data = _misc.GetAllBrands((int)CMS.Infrastructure.BrandType.Supplier);
            string strret = "<select>";
            foreach (BrandDto item in data)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        public ActionResult AddNewBrand(long? bid)
        {
            if (bid != null)
            {
                var data = _misc.LoadBrandDetails((long)bid);
                return View(data);
            }
            else
                return View();
        }
        public ActionResult AddNewSupplier(long? bid)
        {
            if (bid != null)
            {
                var data = _misc.LoadBrandDetails((long)bid);
                return View(data);
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult SaveBrand(BrandDto dto)
        {
            var data = _misc.SaveBrand(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveBrand(long id)
        {
            var data = _misc.DeleteBrand(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(long bid)
        {
            var data = _misc.LoadBrandDetails((long)bid);
            return View(data);
        }
    }
}