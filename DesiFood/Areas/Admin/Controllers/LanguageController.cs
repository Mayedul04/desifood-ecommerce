﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class LanguageController : Controller
    {
        MiscFacade _misc = new MiscFacade();
        // GET: Admin/Language
        public ActionResult Index()
        {
            var data = _misc.GetAllLanguages();
            return View(data);
        }

        public ActionResult AddNew(long? id)
        {
            if (id != null)
            {
                var data = _misc.LoadLanguage((long)id);
                return View(data);
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult SaveLanguage(LanguageDto dto)
        {
            var data = _misc.SaveLanguage(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long id)
        {
            var data = _misc.LoadLanguage((long)id);
            return View(data);
        }
    }
}