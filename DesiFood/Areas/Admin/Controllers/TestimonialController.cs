﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class TestimonialController : Controller
    {
        // GET: Admin/Testimonial
        TestimonialFacade _facade = new TestimonialFacade();
        
        public ActionResult Index()
        {
            var data = _facade.GetAllTestimonials();
            return View(data);
        }

        public ActionResult AddNew(long? tid)
        {
            if (tid != null)
            {
                var data = _facade.LoadTestimonial((long)tid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveList(TestimonialDto dto)
        {
            var data = _facade.SaveTestimonial(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveList(long tid)
        {
            var data = _facade.DeleteTestimonial(tid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long tid)
        {
            var data = _facade.LoadTestimonial((long)tid);
            return View(data);
        }
    }
}