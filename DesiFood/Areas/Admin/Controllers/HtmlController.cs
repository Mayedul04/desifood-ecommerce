﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesiFood.Areas.Admin.Controllers
{
    public class HtmlController : Controller
    {
        HTMLFacade _html = new HTMLFacade();
        GalleryFacade _gallery = new GalleryFacade();
        MiscFacade _misc = new MiscFacade();
        // GET: Admin/Html
        public ActionResult Index()
        {
            var data = _html.GetAllHtmls();
            return View(data);
        }

        public ActionResult AddNew(long? htmlid)
        {
            var languages= _html.GetAllLanguages();
         
            ViewBag.Languages= from  s in languages
                               select new { Id = s.Id, LanguageName = s.ShortName };
            
            if (htmlid != null)
            {
                var data = _html.LoadHTML((long)htmlid);
                return View(data);
            }
            else
                return View();
        }

        [HttpPost]
        public JsonResult SaveHTML(HtmlDto dto)
        {
            var data = _html.SaveHTML(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveHtml(long htmlid)
        {
            var data = _html.DeleteHTML(htmlid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long htmlid)
        {
            var data = _html.LoadHTML((long)htmlid);
            return View(data);
        }
        public ActionResult AddNewContent(long? htmlid, long? contid)
        {
            var galleries = _gallery.GetAllGallery().ToList();
            galleries.Insert(0, null);
            var contenttypes = _misc.GetAllContentTypes("").ToList();
            contenttypes.Insert(0, null);
            ViewBag.ContentTypes = contenttypes;

            ViewBag.Galleries = galleries;
            if (contid != null)
            {
                var data = _html.LoadContent((long)contid);
                return View(data);
            }
            else
            {
                ViewBag.HtmlId = htmlid;
                ViewBag.HtmlTitle = _html.LoadHTML((long)htmlid).Title + " Page";
                return View();
            }
               
        }
        [HttpPost]
        public JsonResult SaveContent(ContentDto dto)
        {
            var data = _html.SaveContent(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContentDetails(long contid)
        {
            var data = _html.LoadContent((long)contid);
            return View(data);
        }

        public JsonResult RemoveContent(long contid)
        {
            var data = _html.DeleteContent(contid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}