﻿using CMS.Admin.Facade;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace DesiFood.Areas.Admin.Controllers
{
    public class SEOController : Controller
    {
        SEOFacade _seo = new SEOFacade();
        HTMLFacade _html = new HTMLFacade();
        // GET: Admin/SEO
        public ActionResult Index()
        {
            var data = _seo.GetAllSEOs();
            return View(data);
        }
        public ActionResult AddNew(PageType? ptype, long? pageid, long? sid)
        {
            var languages = _html.GetAllLanguages();
            ViewBag.Languages = from s in languages
                                select new { Id = s.Id, LanguageName = s.ShortName };
            if (sid != null)
            {
                var data = _seo.LoadSEO((long)sid);
                return View(data);
            }
            else
            {
               var data= _seo.LoadSEOfromPageId((PageType)ptype, (int)pageid);
                return View(data);
            }
        }
        [HttpPost]
        public JsonResult SaveSEO(SEODto dto)
        {
            var data = _seo.SaveSEO(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveSeo(long sid)
        {
            var data = _seo.DeleteSEO(sid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long sid)
        {
            var data = _seo.LoadSEO((long)sid);
            return View(data);
        }
    }
}