import { Component, OnInit } from '@angular/core';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})
export class TestimonialsComponent implements OnInit {

  testimonials:any[];
  slickInit(e) {
    
  }
  slideConfig2 = {"slidesToShow": 1,
   "slidesToScroll": 1,
   autoplay: true,
   arrows:false,
   dots:true,
   autoplaySpeed: 3000
  };
  constructor(private htmlservice: HtmlService) { }

  getData(): void{
    this.htmlservice.getTestimonials().subscribe(results => this.testimonials = results);
  }
  ngOnInit(): void {
    this.getData();
  }


}
