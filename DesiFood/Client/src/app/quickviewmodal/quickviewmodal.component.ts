import {  AfterViewInit, Component,  Input, OnChanges, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { CategoryService } from '../category.service';
import {ModalService} from '../modal.service';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule , Validators} from '@angular/forms';
import {ProductInfo} from '../productInfo';
import * as $ from 'jquery' ;

@Component({
  selector: 'app-quickviewmodal',
  templateUrl: './quickviewmodal.component.html',
  styleUrls: ['./quickviewmodal.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class QuickviewmodalComponent implements OnChanges, AfterViewInit {
  @Input() productId: any;
  oldproductid:any;
  product:ProductInfo;
  shoppingform:FormGroup;
  brands:any[];
  qty:number=1;
  unitprice:any;
  changeDetected = false;

  constructor(private formBuilder: FormBuilder,private catService: CategoryService,private modalservice:ModalService) {
    this.shoppingform = this.formBuilder.group({
      amount:1,
      brand:1,
      qty:1
    });
   }



   getData(prid:any): void{
    this.catService.getProductdetails(prid).subscribe(results => this.product = results);
    this.catService.getProductBrands(prid).subscribe(results => this.brands = results);
  }

 

  ngOnChanges(): void{
    this.doJqueryLoad();
    this.getData(this.productId);
    //this.unitprice=this.product.LatestPrice;
 }
 
 ngAfterViewInit(): void {
    this.doJqueryLoad();
    this.getData(this.productId);
    //this.unitprice=this.product.LatestPrice;
 }
 doJqueryLoad() {
  var qty=1;
  $('.qty span').on('click', function () {
    var qty1 = $(this).closest('.qty').find('input');
    if ($(this).hasClass('qty-add')) {
      qty=qty + 1;
      qty1.val(qty); 
    } else {
      qty=qty - 1;
      return qty > 0 ? qty1.val(qty) : 0;
    }
  });
}
}
