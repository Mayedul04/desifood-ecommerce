import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickviewmodalComponent } from './quickviewmodal.component';

describe('QuickviewmodalComponent', () => {
  let component: QuickviewmodalComponent;
  let fixture: ComponentFixture<QuickviewmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickviewmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickviewmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
