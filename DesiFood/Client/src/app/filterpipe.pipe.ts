import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterpipe'
})
export class FilterpipePipe implements PipeTransform {

 // transform(value: any, ...args: unknown[]): unknown {
   // return null;
 // }
  transform(items: any[], searchText: string, feaured?:boolean, promoted?:boolean, scategories?:any[]): any {
    if (!items) {
      return [];
    }
    if (searchText) {
     searchText = searchText.toLocaleLowerCase();
   
     items= items.filter(it => {
      return it.Name.toLocaleLowerCase().includes(searchText);
      });
    }

    if (feaured) {
      items= items.filter(it2 =>{
        return it2.IsFeatured==feaured;
      });
    }
    if (promoted) {
      items= items.filter(it2 =>{
        return it2.ÓnPromotion==promoted;
      });
    }
    

    if (scategories) {
      let checkedItems:[{1,3}];
     // for (let list of scategories) {
       // checkedItems.push(list.Id);
   // }
      
      items= items.filter(it => {
        return  it.CategoryId.includes(checkedItems);
      });
    }
    
    return items;
    
  }

}
