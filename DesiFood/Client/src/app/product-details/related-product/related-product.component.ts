import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../category.service';



@Component({
  selector: 'app-related-product',
  templateUrl: './related-product.component.html',
  styleUrls: ['./related-product.component.css']
})
export class RelatedProductComponent implements OnInit {
  @Input() catid:number;
  @Output() SelectProductEvent = new EventEmitter<string>();
  //catid:number;
  slickInit(e) {
   
  }
  slideConfig1 = {
    "slidesToShow": 3, 
    "slidesToScroll": 1,
    autoplay: true,
    arrows:true,
    autoplaySpeed: 3000,
    // prevArrow: $('.andro_arrows .slider-prev'),
    // nextArrow: $('.andro_arrows .slider-next'),
  };
  products:any[];
  constructor(private catservice:CategoryService) { 
    
  }

  SelectProduct(value: string) {
    this.SelectProductEvent.emit(value);
  }

  getProducts(): void{
    this.catservice.getRelatedProducts(this.catid).subscribe(results => this.products = results);
  }
  ngOnInit(): void {
    console.log(this.catid);
    this.getProducts();
  }

}
