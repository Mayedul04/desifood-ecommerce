import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { HtmlService } from '../html.service';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule , Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  prodid:any;
  productId: number;
  product:any;
  shoppingform:FormGroup;
  commentform:FormGroup;
  details:any;
  additional:any;
  reviews:any;
  healthbenefits:any;
  usage:any;
  recipe:any;
  storage:any;
  category:number;

  constructor(private catservice:CategoryService,private formBuilder: FormBuilder,private htmlservice:HtmlService,private route: ActivatedRoute) {
    
    this.shoppingform = this.formBuilder.group({
      amount:0,
      brand:1,
      qty:0
    });
    this.commentform = this.formBuilder.group({
      Name:'',
      Email:'',
      Comment:''
    });
    this.prodid = this.route.snapshot.params['id'];
   }

   GetPreviewItem(newItem: number) {
    this.productId=newItem;
  }


   getData(prid:number): void{
    this.catservice.getProductdetails(prid).subscribe(results => this.product = results);
    this.catservice.getSingelContent(prid,4).subscribe(results => this.details = results);
    this.catservice.getSingelContent(prid,5).subscribe(results => this.additional = results);
    this.catservice.getSingelContent(prid,10).subscribe(results => this.healthbenefits = results);
    this.catservice.getSingelContent(prid,11).subscribe(results => this.usage = results);
    this.catservice.getSingelContent(prid,12).subscribe(results => this.recipe = results);
    this.catservice.getSingelContent(prid,13).subscribe(results => this.storage = results);
  }

  

  ngOnInit(): void {
    
    this. getData(this.prodid);
   
  }

}
