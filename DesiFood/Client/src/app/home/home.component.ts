import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { CategoryService } from '../category.service';
import { HtmlService } from '../html.service';
import {ModalService} from '../modal.service';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule , Validators} from '@angular/forms';


import * as $ from 'jquery' ;
//declare var $:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None,
  
})
export class HomeComponent implements OnInit {
  
  productId: number;
  newsletterform:FormGroup;
  shoppingform:FormGroup;
  Ifres:boolean;
  clientData:any;
  response:any;
  responsemsg:string;
  newdata:number;
 
  constructor(private catService: CategoryService,private formBuilder: FormBuilder,private htmlService: HtmlService,private modalservice:ModalService) {
    this.newsletterform = this.formBuilder.group({
      Email:['',[Validators.email, Validators.required ]],
      SubjectId:3
    });
    this.shoppingform = this.formBuilder.group({
      amount:1,
      brand:1,
      qty:0
    });
   }

   getModalData(): void{
    this.modalservice.getSelectedProduct().subscribe(data => this.productId = data);
  }
  GetPreviewItem(newItem: number) {
    this.productId=newItem;
  }
  

  ngOnInit(): void {
   this.productId=1;
    this.Ifres=false;
    
  }
  onSubmit() {
    // Process checkout data here
    this.clientData = this.newsletterform.value;
    this.htmlService.sendMessage(this.clientData).subscribe(results => this.response = results);
    this.Ifres=true;
    this.responsemsg=this.response.Message;
    this.newsletterform.reset();
  }
}
