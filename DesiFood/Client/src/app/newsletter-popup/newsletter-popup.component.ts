import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule , Validators} from '@angular/forms';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-newsletter-popup',
  templateUrl: './newsletter-popup.component.html',
  styleUrls: ['./newsletter-popup.component.css']
})
export class NewsletterPopupComponent implements OnInit {
  contactusform:FormGroup;
  clientData:any;
  responsemsg:string;

  constructor(private htmlService: HtmlService,private formBuilder: FormBuilder,) {
    this.contactusform = this.formBuilder.group({
      Email:['',[Validators.email, Validators.required ]],
      SubjectId:3
    });
   }

  ngOnInit(): void {
  }
  onSubmit() {
    // Process checkout data here
    this.clientData = this.contactusform.value;
    this.htmlService.sendMessage(this.clientData).subscribe(results => this.responsemsg = results);
    this.contactusform.reset();
  }
}
