export interface Content {
    Id: number;
    Title: string;
    SmallText: string;
    ImagePath:string;
    BigText:string;
    Link:string;
}
