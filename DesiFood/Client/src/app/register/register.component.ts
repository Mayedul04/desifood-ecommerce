import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Content } from '../content';
import { HtmlService } from '../html.service';
import { CustomerService } from '../customer.service';
import { ConfirmPasswordValidator } from '../confirm-password.validator';
import {Router} from '@angular/router';

import { flatMap, result } from 'lodash';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  welcome: Content = <Content>{};
  registerForm:FormGroup;
  responsemsg:any;
  clientData:any;
  isFail:boolean=false;
  submitted: boolean = false;
  

  constructor(private htmlservice:HtmlService,private formBuilder: FormBuilder,private customerservice:CustomerService, private router:Router) {
    this.registerForm = this.formBuilder.group({
      UserName: ['', Validators.required],
      Password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      Email:['',[Validators.email, Validators.required ]]
    },
    {
      validator: ConfirmPasswordValidator("Password", "confirmPassword")
    }
    );

   }

  getWelcomeData(): void{
    this.htmlservice.getSingleContent(4,6).subscribe(results => this.welcome = results);
  }
  ngOnInit(): void {
    this.getWelcomeData()
    
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.valid) {
      // Process checkout data here
    this.clientData = this.registerForm.value;
    this.customerservice.Register(this.clientData).subscribe(results => this.responsemsg = results);
    if(this.responsemsg.Success)
         {
           this.isFail=false;
          this.router.navigate(['/login'])
         }
         else this.isFail=true;
  }
   
  }
}
