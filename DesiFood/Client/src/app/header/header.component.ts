import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule , Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  searchform:FormGroup;
  responsemsg:string;
  clientData:any;

  constructor(private htmlService: HtmlService,private formBuilder: FormBuilder,private router: Router) {
    this.searchform = this.formBuilder.group({
      searchstring: '',
      
    });
   }

  ngOnInit(): void {
  }
  navigate() {
    // Process checkout data here
    this.clientData = this.searchform.value;
    //this.htmlService.sendMessage(this.clientData).subscribe(results => this.responsemsg = results);
    this.router.navigate(['/shop/'+ this.clientData.searchstring]);
  }
}
