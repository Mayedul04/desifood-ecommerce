import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {
  name = new FormControl('');
  constructor() { }
  updateName() {
    this.name.setValue('Nancy');
  }
  ngOnInit(): void {
  }

}
