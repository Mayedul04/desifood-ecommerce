import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../category.service';

@Component({
  selector: 'app-header-category',
  templateUrl: './header-category.component.html',
  styleUrls: ['./header-category.component.css']
})
export class HeaderCategoryComponent implements OnInit {

  categories: any[];


  constructor(private catservice: CategoryService) { }

  getCategories(): void{
    this.catservice.getCategories().subscribe(results => this.categories = results);
  }

  ngOnInit(): void {
    this.getCategories();
  }

}
