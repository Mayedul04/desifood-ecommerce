import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePromotextComponent } from './home-promotext.component';

describe('HomePromotextComponent', () => {
  let component: HomePromotextComponent;
  let fixture: ComponentFixture<HomePromotextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePromotextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePromotextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
