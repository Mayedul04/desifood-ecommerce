import { Content } from '../content';
import { Component, OnInit } from '@angular/core';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-home-promotext',
  templateUrl: './home-promotext.component.html',
  styleUrls: ['./home-promotext.component.css']
})
export class HomePromotextComponent implements OnInit {

  
  promotext: Content = <Content>{};

  constructor(private htmlservice: HtmlService) { }

  getData(): void{
    this.htmlservice.getPromoText().subscribe(results => this.promotext = results);
  }
  ngOnInit(): void {
    this.getData();
  }

}
