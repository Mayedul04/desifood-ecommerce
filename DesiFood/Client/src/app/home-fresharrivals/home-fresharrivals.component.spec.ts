import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFresharrivalsComponent } from './home-fresharrivals.component';

describe('HomeFresharrivalsComponent', () => {
  let component: HomeFresharrivalsComponent;
  let fixture: ComponentFixture<HomeFresharrivalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeFresharrivalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeFresharrivalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
