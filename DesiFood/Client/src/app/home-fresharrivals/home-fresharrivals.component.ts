import { Component, OnInit,EventEmitter, Input, Output } from '@angular/core';
import {ModalService} from '../modal.service';
import { CategoryService } from '../category.service';
declare var $: any;

@Component({
  selector: 'app-home-fresharrivals',
  templateUrl: './home-fresharrivals.component.html',
  styleUrls: ['./home-fresharrivals.component.css']
})
export class HomeFresharrivalsComponent implements OnInit {
  @Output() SelectProductEvent = new EventEmitter<string>();
  products:any[];
  slickInit(e) {
    
  }
  slideConfig1 = {
    "slidesToShow": 3, 
    "slidesToScroll": 1,
    autoplay: true,
    arrows:true,
    autoplaySpeed: 3000,
    // prevArrow: $('.andro_arrows .slider-prev'),
    // nextArrow: $('.andro_arrows .slider-next'),
  };
  constructor(private productservice: CategoryService,public modalService: ModalService) { }
  

  SelectProduct(value: string) {
    this.SelectProductEvent.emit(value);
  }

  getProducts(): void{
    this.productservice.getFreshArrivals().subscribe(results => this.products = results);
  }
  ngOnInit(): void {
    this.getProducts();
  }

}
