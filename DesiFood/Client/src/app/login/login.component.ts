import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Content } from '../content';
import { HtmlService } from '../html.service';
import { CustomerService } from '../customer.service';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  welcome: Content = <Content>{};
  loginForm:FormGroup;
  responsemsg:any;
  clientData:any;
  submitted: boolean = false;
  isFail:boolean=false;

  constructor(private htmlservice:HtmlService,private formBuilder: FormBuilder,private customerservice:CustomerService,private cookieservice: CookieService, private router:Router) {
    this.loginForm = this.formBuilder.group({
      UserName: ['', Validators.required],
      Password: ['', Validators.required]
    });

   }

   getWelcomeData(): void{
    this.htmlservice.getSingleContent(5,6).subscribe(results => this.welcome = results);
  }
  ngOnInit(): void {
    this.getWelcomeData()
    
  }
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.valid) {
      // Process checkout data here
    this.clientData = this.loginForm.value;
    this.customerservice.Login(this.clientData).subscribe(results => this.responsemsg = results);
    if(this.responsemsg.Success)
    {
      const dateNow = new Date();
    dateNow.setDate(dateNow.getDate() + 30);
    this.cookieservice.set('Id',this.responsemsg.Id,dateNow);
    this.cookieservice.set('Customer',this.responsemsg.UserName,dateNow);
    this.cookieservice.set('WishId',this.responsemsg.MainCartId,dateNow);
    this.cookieservice.set('MainId',this.responsemsg.WishCartId,dateNow);
    this.isFail=false;
    this.router.navigate(['/home'])
    }
    else this.isFail=true;
    
  }
   
  }
}
