import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Content } from './content';

@Injectable({
  providedIn: 'root'
})
export class HtmlService {
  private apilink= 'https://localhost:44340/api/html/';

  constructor(private http: HttpClient) { }

  getHomeBanner(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'homebanners');
  }

  getHomeFeatures(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'homefeatures');
  }
  getTestimonials(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'gettestimonials');
  }
  getBrands(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'getbrands');
  }

  getPromoText(): Observable<Content> {
    return this.http.get<any>(this.apilink+'promotext');
  }

  getAboutwelcome(): Observable<Content> {
    return this.http.get<any>(this.apilink+'about-welcome');
  }
  getAboutDetails(): Observable<Content> {
    return this.http.get<any>(this.apilink+'about-details');
  }
  getContactInfo(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'contact-details');
  }

  getListContent(hid:number,ctype:number): Observable<any[]> {
    return this.http.get<any>(this.apilink+'getlistcontents' + '?htmlid=' + hid +'&ctype='+ctype);
  }

  getSingleContent(hid:number,ctype:number): Observable<Content> {
    return this.http.get<any>(this.apilink+'getsinglecontent' + '?htmlid=' + hid +'&ctype='+ctype);
  }

  getBranches(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'branches');
  }
  getGallery(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'gallery'+'?gid=1');
  }

  sendMessage(data:Observable<any[]>): Observable<string>{
    return this.http.post<any>(this.apilink+'sendmessage',data).pipe();
  }

  getContactSubjects(): Observable<any[]> {
    return this.http.get<any>(this.apilink+'csubjects');
  }
}
