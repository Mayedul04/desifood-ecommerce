import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeProductSummaryComponent } from './home-product-summary.component';

describe('HomeProductSummaryComponent', () => {
  let component: HomeProductSummaryComponent;
  let fixture: ComponentFixture<HomeProductSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeProductSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeProductSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
