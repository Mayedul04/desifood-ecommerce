import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-home-product-summary',
  templateUrl: './home-product-summary.component.html',
  styleUrls: ['./home-product-summary.component.css']
})
export class HomeProductSummaryComponent implements OnInit {

  categories: any[];

  constructor(private catservice: CategoryService) { }

  getCategorySummary(): void{
    this.catservice.getCategorySummary().subscribe(results => this.categories = results);
  }

  ngOnInit(): void {
    this.getCategorySummary();
  }

}
