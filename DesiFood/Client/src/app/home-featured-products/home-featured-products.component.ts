import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-home-featured-products',
  templateUrl: './home-featured-products.component.html',
  styleUrls: ['./home-featured-products.component.css']
})
export class HomeFeaturedProductsComponent implements OnInit {
  @Output() SelectProductEvent = new EventEmitter<string>();
  products:any[];

  constructor(private productservice: CategoryService) { }
 
  SelectProduct(value: string) {
    this.SelectProductEvent.emit(value);
  }

  getProducts(): void{
    this.productservice.getFeaturedProducts().subscribe(results => this.products = results);
  }
  ngOnInit(): void {
    this.getProducts();
  }

}
