import { Component,  OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { HtmlService } from '../html.service';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule , Validators, FormArray} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
 
  searchText = '';
  featured:false;
  promoted:false;
  scategories:any[];
  
  products:any[];
  searchform:FormGroup;
  sortingform:FormGroup;
  categories:any[];
  categorydata:any[];
  brands:any[];
  selectedSort:string="'asc':'Name'";
  selectedProductId:number;

  get ordersFormArray() {
    return this.searchform.controls.categories as FormArray;
  }

  constructor(private catservice:CategoryService,private formBuilder: FormBuilder,private htmlservice:HtmlService,private route: ActivatedRoute) { 
    this.sortingform = this.formBuilder.group({
      orderby:''
    });
    this.searchform = this.formBuilder.group({
      searchText:'',
      categories: new FormArray([])
    });
    
  }
 
  private addCheckboxes() {
    this.categorydata.forEach(() => this.ordersFormArray.push(new FormControl(false)));
  }
  
   getSe(): any[]{
    return this.searchform.value.categories
      .map((checked, i) => checked ? this.categorydata[i].Id : null)
      .filter(v => v !== null);
  }

  SelectProduct(value: number) {
    this.selectedProductId=value;
  }
  

  getInitialData(): void{
    this.catservice.getCategories().subscribe(results => this.categorydata = results);
    //this.addCheckboxes();
    this.htmlservice.getBrands().subscribe(results => this.brands = results);
    this.catservice.getAllProducts().subscribe(results => this.products = results);
  }

  ngOnInit(): void {
    this.getInitialData();
  }

}
