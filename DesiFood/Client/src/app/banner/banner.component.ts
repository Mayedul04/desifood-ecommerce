import { Component, OnInit } from '@angular/core';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  
  banners: any[];
  slickInit(e) {
    
  }
  slideConfig = {"slidesToShow": 1, "slidesToScroll": 1,autoplay: true,autoplaySpeed: 3000,};
  constructor(private htmlservice: HtmlService) { }
  
  getBanners(): void{
    this.htmlservice.getHomeBanner().subscribe(results => this.banners = results);
  }
  ngOnInit(): void {
    this.getBanners();
  }

}
