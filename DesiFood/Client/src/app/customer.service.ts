import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private apilink= 'https://localhost:44340/api/customer/';
  constructor(private http: HttpClient) { }


  Register(data:Observable<any[]>): Observable<string>{
    return this.http.post<any>(this.apilink+'register',data).pipe();
  }

  Login(data:Observable<any[]>): Observable<string>{
    return this.http.post<any>(this.apilink+'login',data).pipe();
  }
}
