import { Component, OnInit } from '@angular/core';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-home-features',
  templateUrl: './home-features.component.html',
  styleUrls: ['./home-features.component.css']
})
export class HomeFeaturesComponent implements OnInit {
  Title: string;
  SmallDetails: string;
  features: any[];
  constructor(private htmlservice: HtmlService) { }

  getContents(): void{
    this.htmlservice.getHomeFeatures().subscribe(results => this.features = results);
  }
  ngOnInit(): void {
    this.getContents();
  }
  

}
