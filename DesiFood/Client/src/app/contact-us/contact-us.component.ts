import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule , Validators} from '@angular/forms';
import { strict } from 'assert';
import { HtmlService } from '../html.service';


@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contacts: any[];
  branches:any[];
  faqs:any[];
  csubjects:any[];
  contactusform:FormGroup;
  responsemsg:string;
  clientData:any;

  constructor(private htmlService: HtmlService,private formBuilder: FormBuilder) {
    this.contactusform = this.formBuilder.group({
      FirstName: '',
      LastName: '',
      Email:['',[Validators.email, Validators.required ]],
      Message:'',
      SubjectId:1
    });
   }

  getData(): void{
    this.htmlService.getContactInfo().subscribe(results => this.contacts = results);
    this.htmlService.getBranches().subscribe(results => this.branches = results);
    this.htmlService.getListContent(3,9).subscribe(results => this.faqs = results);
    this.htmlService.getContactSubjects().subscribe(data => this.csubjects = data);
  }

  ngOnInit(): void {
    this.getData();
  }
  onSubmit() {
    // Process checkout data here
    this.clientData = this.contactusform.value;
    this.htmlService.sendMessage(this.clientData).subscribe(results => this.responsemsg = results);
    this.contactusform.reset();
  }
}
