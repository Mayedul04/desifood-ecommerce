export interface Category {
    Id: number;
    CategoryName: string;
    ParentName: string;
    ThumbImagePath: string;
  }