import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-home-product-category',
  templateUrl: './home-product-category.component.html',
  styleUrls: ['./home-product-category.component.css']
})
export class HomeProductCategoryComponent implements OnInit {
  categories: any[];

  constructor(private catservice: CategoryService) { }

  getCategories(): void{
    this.catservice.getCategories().subscribe(results => this.categories = results);
  }

  ngOnInit(): void {
    this.getCategories();
  }
}
