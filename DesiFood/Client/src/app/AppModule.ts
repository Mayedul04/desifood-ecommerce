import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common'; 
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { GalleryComponent } from './gallery/gallery.component';
import { BannerComponent } from './banner/banner.component';
import {CookieService} from 'ngx-cookie-service';
import { HomeFeaturesComponent } from './home-features/home-features.component';
import { HeaderCategoryComponent } from './header/header-category/header-category.component';
import { CartSummaryComponent } from './header/cart-summary/cart-summary.component';
import { SearchFormComponent } from './header/search-form/search-form.component';
import { NewsletterPopupComponent } from './newsletter-popup/newsletter-popup.component';
import { HomeSidebarComponent } from './home-sidebar/home-sidebar.component';
import { PopularCategoryComponent } from './home-sidebar/popular-category/popular-category.component';
import { HomeProductCategoryComponent } from './home-product-category/home-product-category.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { HomeFresharrivalsComponent } from './home-fresharrivals/home-fresharrivals.component';
import { HomeFeaturedProductsComponent } from './home-featured-products/home-featured-products.component';
import { HomeProductSummaryComponent } from './home-product-summary/home-product-summary.component';
import { HomePromotextComponent } from './home-promotext/home-promotext.component';
import { HomePromoProductsComponent } from './home-promo-products/home-promo-products.component';
import { HomeInstagramComponent } from './home-instagram/home-instagram.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { BrandsComponent } from './brands/brands.component';
import { AppAboutComponent } from './app-about/app-about.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import {CategoryComponent} from './category/category.component';
import {ProductDetailsComponent} from './product-details/product-details.component' ;
import {ShopComponent} from './shop/shop.component';
import {RelatedProductComponent} from './product-details/related-product/related-product.component';
import {FilterpipePipe} from './filterpipe.pipe';
import {SortByPipe} from './sort-by.pipe';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { QuickviewmodalComponent } from './quickviewmodal/quickviewmodal.component';
import {ModalService} from './modal.service';


const appRoutes: Routes = [
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'about', component: AppAboutComponent },
  { path: 'category/:id', component: CategoryComponent },
  { path: 'product/:id', component: ProductDetailsComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'shop/:catid', component: ShopComponent },
  { path: 'shop/:search', component: ShopComponent },
  { path: 'home', component: HomeComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    GalleryComponent,
    BannerComponent,
    HomeFeaturesComponent,
    HeaderCategoryComponent,
    CartSummaryComponent,
    SearchFormComponent,
    NewsletterPopupComponent,
    HomeSidebarComponent,
    PopularCategoryComponent,
    HomeProductCategoryComponent,
    HomeFresharrivalsComponent,
    HomeFeaturedProductsComponent,
    HomeProductSummaryComponent,
    HomePromotextComponent,
    HomePromoProductsComponent,
    HomeInstagramComponent,
    TestimonialsComponent,
    BrandsComponent,
    AppAboutComponent,
    HomeComponent,
    ContactUsComponent,
    CategoryComponent,
    ProductDetailsComponent,
    ShopComponent,
    PageNotFoundComponent,
    RelatedProductComponent,
    FilterpipePipe,
    SortByPipe,
    RegisterComponent,
    LoginComponent,
    QuickviewmodalComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    SlickCarouselModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  
  providers: [ModalService,CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
