import { Component, OnInit } from '@angular/core';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  images:any;

  constructor(private htmlservice: HtmlService) { }

  getData(): void{
    this.htmlservice.getGallery().subscribe(results => this.images = results);
  }
  ngOnInit(): void {
    this.getData();
  }

}
