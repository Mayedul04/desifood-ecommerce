import { Component, OnInit } from '@angular/core';
import { Content } from '../content';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-app-about',
  templateUrl: './app-about.component.html',
  styleUrls: ['./app-about.component.css']
})
export class AppAboutComponent implements OnInit {

  welcome: Content = <Content>{};
  details: Content = <Content>{};
  constructor(private htmlservice:HtmlService) { }

  getWelcomeData(): void{
    this.htmlservice.getAboutwelcome().subscribe(results => this.welcome = results);
  }
  getDetailsData(): void{
    this.htmlservice.getAboutDetails().subscribe(results => this.details = results);
  }
  ngOnInit(): void {
    this.getWelcomeData();
    this.getDetailsData();
  }

}
