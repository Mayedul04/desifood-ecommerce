import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import {jquery} from 'jquery.countdown'
import * as $ from 'jquery' ;

@Component({
  selector: 'app-home-promo-products',
  templateUrl: './home-promo-products.component.html',
  styleUrls: ['./home-promo-products.component.css']
})
export class HomePromoProductsComponent implements OnInit {
  products:any[];
  slickInit(e) {
    
  }
  slideConfig = {
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: false

  };
  
  constructor(private productservice: CategoryService) { }

  getProducts(): void{
    this.productservice.getPromotionalProducts().subscribe(results => this.products = results);
  }
  ngOnInit(): void {
    this.getProducts();
  }


}
