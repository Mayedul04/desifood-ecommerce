import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePromoProductsComponent } from './home-promo-products.component';

describe('HomePromoProductsComponent', () => {
  let component: HomePromoProductsComponent;
  let fixture: ComponentFixture<HomePromoProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePromoProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePromoProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
