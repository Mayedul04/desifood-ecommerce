import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-home-sidebar',
  templateUrl: './home-sidebar.component.html',
  styleUrls: ['./home-sidebar.component.css']
})
export class HomeSidebarComponent implements OnInit {
  categories: any[];
  constructor(private catservice: CategoryService) { }

  getCategories(): void{
    this.catservice.getCategories().subscribe(results => this.categories = results);
  }

  ngOnInit(): void {
    this.getCategories();
  }

}
