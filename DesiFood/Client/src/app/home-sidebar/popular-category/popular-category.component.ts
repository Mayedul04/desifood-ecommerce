import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../category.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-popular-category',
  templateUrl: './popular-category.component.html',
  styleUrls: ['./popular-category.component.css']
})
export class PopularCategoryComponent implements OnInit {
  categories: any[];

  constructor(private catservice: CategoryService) { }

  getCategories(): void{
    this.catservice.getCategories().subscribe(results => this.categories = results);
  }

  ngOnInit(): void {
    this.getCategories();
  }


}
