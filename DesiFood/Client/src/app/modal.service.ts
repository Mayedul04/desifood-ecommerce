import {  Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
 // Observable 
 private productid:any=1;
  
   // Service message commands
  SetSelectedProduct(proid: any) {
    this.productid=proid;
  }

  getSelectedProduct():Observable<any> {
    return of(this.productid);
    
  }
}
