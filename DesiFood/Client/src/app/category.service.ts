import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private apilink= 'https://localhost:44340/api/products/';
  
  constructor(private http: HttpClient) { }

  getCategories(): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'categories');
  }
  getFreshArrivals(): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'fresharrival');
  }
  getFeaturedProducts(): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'featured');
  }

  getCategorySummary(): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'categorysummary');
  }

  getPromotionalProducts(): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'promotionalproducts');
  }

  getAllProducts(): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'allproducts');
  }

  getRelatedProducts(catid:number): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'relatedproducts'+'?catid='+catid);
  }
  getProductdetails(prodid:number): Observable<any> {
    return this.http.get<any>(this.apilink+ 'product-details'+'?prodid='+prodid);
  }

  getSingelContent(prodid:number,ctype:number): Observable<any> {
    return this.http.get<any>(this.apilink+ 'getsinglecontent'+'?prodid='+prodid + '&ctype='+ctype);
  }

  getProductBrands(prodid:number): Observable<any[]> {
    return this.http.get<any>(this.apilink+ 'getproductbrands'+'?prodid='+prodid);
  }
}
