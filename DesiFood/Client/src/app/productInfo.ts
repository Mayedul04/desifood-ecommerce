export interface ProductInfo {
    ProductId: number;
    CategoryId:number;
    ProductCategoryName:string;
    Alias:string;
    Name: string;
    BasePrice: number;
    ThumbImagePath:string;
    LatestPrice:string;
    UoMText:string;
    IsFeatured:boolean;
    SmallDetails:string;
    BrandName:string;
    EntryDate:Date;
    ExpiryDate:Date;
    
}
