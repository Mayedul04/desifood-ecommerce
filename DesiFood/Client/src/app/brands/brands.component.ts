import { Component, OnInit } from '@angular/core';
import { HtmlService } from '../html.service';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.css']
})
export class BrandsComponent implements OnInit {
  brands:any[];

  constructor(private htmlservice: HtmlService) { }

  getData(): void{
    this.htmlservice.getBrands().subscribe(results => this.brands = results);
  }
  ngOnInit(): void {
    this.getData();
  }

}
