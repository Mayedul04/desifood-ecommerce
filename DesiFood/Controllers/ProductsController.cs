﻿using CMS.Admin.Facade;
using CMS.Dto;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesiFood.Controllers
{
    public class ProductsController : ApiController
    {
        ProductFacade _prod = new ProductFacade();
        PromotionFacade _promo = new PromotionFacade();
       
        [System.Web.Http.Route("api/products/categories")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetCategories()
        {
            var data = _prod.GetAllCategory(1);
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
        [System.Web.Http.Route("api/products/categorydetails")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage LoadCategory(int id)
        {
            var product = _prod.LoadCategory(id);
            if (product == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, product);
            }
            return Request.CreateResponse(HttpStatusCode.OK, product);
        }

        [System.Web.Http.Route("api/products/categoryproducts")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage ProductsByCategory(int catid)
        {
            
            var product = _prod.GetAllChildProductByCategory(catid);
            if (product == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, product);
            }
            return Request.CreateResponse(HttpStatusCode.OK, product);
        }

        [System.Web.Http.Route("api/products/categorysummary")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage ProductSummaryByCategory()
        {
            
            var product = _prod.ProductCountByCategory();
            if (product == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, product);
            }
            return Request.CreateResponse(HttpStatusCode.OK, product);
        }

        [System.Web.Http.Route("api/products/fresharrival")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage ProductsByFreshness()
        {
            var product = _prod.GetFreshArrivals();
            if (product == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, product);
            }
            return Request.CreateResponse(HttpStatusCode.OK, product);
        }

        [System.Web.Http.Route("api/products/featured")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage FeaturedProducts()
        {
            var product = _prod.GetAllFeaturedProducts();
            if (product == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, product);
            }
            return Request.CreateResponse(HttpStatusCode.OK, product);
        }

        [System.Web.Http.Route("api/products/promotionalproducts")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage PromotionalProducts()
        {
            var products = _promo.GetPromotionalProducts();
            if (products == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, products);
            }
            return Request.CreateResponse(HttpStatusCode.OK, products);
        }

        [System.Web.Http.Route("api/products/relatedproducts")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage RelatedProducts(long catid)
        {
            var products = _prod.GetMultiPurposeProducts().Where(x=>x.CategoryId==catid);
            if (products == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, products);
            }
            return Request.CreateResponse(HttpStatusCode.OK, products);
        }

        [System.Web.Http.Route("api/products/allproducts")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAllProducts()
        {
            var products = _prod.GetMultiPurposeProducts();
            if (products == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, products);
            }
            return Request.CreateResponse(HttpStatusCode.OK, products);
        }

        [System.Web.Http.Route("api/products/product-details")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage ProductDetailss(long prodid)
        {
            var products = _prod.GetMultiPurposeSingleProduct(prodid);
            if (products == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, products);
            }
            return Request.CreateResponse(HttpStatusCode.OK, products);
        }

        [System.Web.Http.Route("api/products/getsinglecontent")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetSingleContent(long prodid, int ctype)
        {
            var data = _prod.GetProductContents(prodid).Where(x => x.ContentType == ctype).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/products/getproductbrands")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAllBrandsbyProducts(long prodid)
        {
            var suppliers = _prod.GetBrandsByProduct(prodid);
            if (suppliers == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, suppliers);
            }
            return Request.CreateResponse(HttpStatusCode.OK, suppliers);
        }

        
    }
}
