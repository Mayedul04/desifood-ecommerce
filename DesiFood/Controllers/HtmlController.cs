﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesiFood.Controllers
{
    public class HtmlController : ApiController
    {
        HTMLFacade _html = new HTMLFacade();
        TestimonialFacade testi = new TestimonialFacade();
        MiscFacade _misc = new MiscFacade();
        GalleryFacade _gallery = new GalleryFacade();
        BranchFacade _branch = new BranchFacade();
        ContactFacade _contact = new ContactFacade();
        [System.Web.Http.Route("api/html/homebanners")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetHomeBanners()
        {
            var data = _html.GetAllHtmlContents(1).Where(x=>x.ContentType==1).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
        [System.Web.Http.Route("api/html/homefeatures")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetHomeFeatures()
        {
            var data = _html.GetAllHtmlContents(1).Where(x => x.ContentType == 2).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/promotext")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetPromotext()
        {
            var data = _html.GetAllHtmlContents(1).Where(x => x.ContentType == 3).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/gettestimonials")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetTestimonials()
        {
            var data = testi.GetAllTestimonials();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/getbrands")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetSuppliers()
        {
            var data = _misc.GetAllBrands(2);
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/gallery")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetGalleryItems(long gid)
        {
            var data = _gallery.LoadGalleryItems(gid);
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/about-welcome")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAboutWelcome()
        {
            var data = _html.GetAllHtmlContents(2).Where(x => x.ContentType == 6).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/about-details")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAboutDetails()
        {
            var data = _html.GetAllHtmlContents(2).Where(x => x.ContentType == 7).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/contact-details")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetCaontactDetails()
        {
            var data = _html.GetAllHtmlContents(3).Where(x => x.ContentType == 8).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
        [System.Web.Http.Route("api/html/branches")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetOffices()
        {
            var data = _branch.GetAllLists();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/csubjects")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetContactSubjects()
        {
            var data = _misc.GetAllContactSubjects();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/getlistcontents")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetListOfContents(long htmlid, int ctype)
        {
            var data = _html.GetAllHtmlContents(htmlid).Where(x => x.ContentType == ctype).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/html/getsinglecontent")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetSingleContent(long htmlid, int ctype)
        {
            var data = _html.GetAllHtmlContents(htmlid).Where(x => x.ContentType == ctype).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
        [System.Web.Http.Route("api/html/sendmessage")]
       
        [HttpPost]
        public HttpResponseMessage SaveMessage(ContactRequestDto dto)
        {
            dto.Action = CMS.Infrastructure.EmailAction.Received;
            dto.ContactDate = DateTime.Now;
            dto.BranchName = _branch.LoadList(1).BranchName;
            dto.ContactDateText = ((DateTime)dto.ContactDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            var data = _contact.SaveContactMessage(dto);
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
    }
}
