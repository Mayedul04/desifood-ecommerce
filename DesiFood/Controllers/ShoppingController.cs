﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesiFood.Controllers
{
    public class ShoppingController : ApiController
    {
        ShoppingCartFacade _shop = new ShoppingCartFacade();
        [System.Web.Http.Route("api/shopping/addtocart")]
        [HttpPost]
        public HttpResponseMessage AddItemtoCart(ShoppingCartDetailDto dto)
        {
            var data = _shop.SaveItemToCart(dto);
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
    }
}
