﻿using CMS.Admin.Facade;
using CMS.Dto;
using CMS.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesiFood.Controllers
{
    public class CustomerController : ApiController
    {
        CustomerFacade _cust = new CustomerFacade();
        [System.Web.Http.Route("api/customer/register")]
        [HttpPost]
        public HttpResponseMessage SaveRegister(CustomerDto dto)
        {
            var data = _cust.SaveCustomer(dto);
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [System.Web.Http.Route("api/customer/login")]
        [HttpPost]
        public HttpResponseMessage Login(CustomerDto dto)
        {
            CustomerDto vm = new CustomerDto();
            vm = _cust.DoLogin(dto);
            if (vm.Id > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, vm);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, vm);
            }
           
        }
        [System.Web.Http.Route("api/customer/logout")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage Logout()
        {
            SessionHelper.DeleteCookie();
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
    }
}
