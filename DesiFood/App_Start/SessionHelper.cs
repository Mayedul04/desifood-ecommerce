﻿using CMS.Admin.Facade;
using CMS.Dto;
using CMS.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CMS.UI
{
    public static class SessionHelper
    {
        
        public static UserResourceDto UserProfile
        {
            get { return GetFromSession<UserResourceDto>("UserProfile"); }
            set { SetInSession("UserProfile", value); }
        }

        private static object GetFromSession(string key)
        {
            return HttpContext.Current.Session[key];
        }
        private static T GetFromSession<T>(string key)
        {
            return (T)HttpContext.Current.Session[key];
        }
        public static void SetInSession(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }
        public static void SetCookie(UserResourceDto user, int cookieExpireDate = 30)
        {
            string CookieName = "CMS_User";
            HttpCookie myCookie = new HttpCookie(CookieName);
            myCookie["UserId"] = user.UserId.ToString();
            myCookie["UserName"] = user.UserName.ToString();
            myCookie.Expires = DateTime.Now.AddDays(cookieExpireDate);
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }
        public static long GetCookie()
        {
            HttpCookie myCookie = HttpContext.Current.Request.Cookies["CMS_User"];
            if (myCookie != null)
            {
                long userId = Convert.ToInt32(myCookie.Values["UserId"]);
                
                return userId;
            }
            return 0;
        }

        public static void DeleteCookie()
        {
            HttpCookie myCookie = HttpContext.Current.Request.Cookies["CMS_User"];
            myCookie.Expires = DateTime.Now.AddDays(-1);
        }
    }
}