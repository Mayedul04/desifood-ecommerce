﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace DesiFood.App_Start
{
    public class AutoMapperBootstrapper
    {
        public static void BootStrapAutoMaps()
        {
            DefaultMappings();
        }

        private static void DefaultMappings()
        {
            //GenService _service = new GenService();

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new CMS.AutoMaps.CMSMappingProfile());

            });

        }
    }
}