﻿using System.Web;
using System.Web.Optimization;

namespace DesiFood
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/vendors/jquery-3.3.1.min.js",
                        "~/vendors/jquery.easing.1.3.min.js",
                        "~/vendors/jquery.parallax-1.1.3.min.js"));



            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/vendors/modernizr.js"));

            bundles.Add(new ScriptBundle("~/bundles/vendorsjs").Include(
                        "~/vendors/monkeysan.jquery.nav.1.0.min.js",
                        "~/vendors/monkeysan.tabs.min.js",
                        "~/vendors/handlebars-v4.0.5.min.js",
                        "~/vendors/owl-carousel/owl.carousel.min.js",
                        "~/vendors/fancybox/jquery.fancybox.min.js",
                        "~/vendors/isotope.pkgd.min.js",
                        "~/vendors/monkeysan.validator.min.js",
                        "~/vendors/arcticmodal/jquery.arcticmodal-0.3.min.js",
                        "~/vendors/retina.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/revolution").Include(
                        "~/vendors/revolution/js/jquery.themepunch.tools.min.js",
                        "~/vendors/revolution/js/jquery.themepunch.revolution.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/madjs").Include(
                        "~/js/modules/mad.alert-box.min.js",
                        "~/js/modules/mad.contact-form.js",
                        "~/js/modules/mad.sticky-header-section.min.js",
                        "~/js/modules/mad.isotope.js",
                        "~/js/mad.app.js"));

            bundles.Add(new StyleBundle("~/bundles/bootstrapcss").Include(
                      "~/css/bootstrap.min.css",
                      "~/css/animate.min.css",
                      "~/css/linearicons.css"));

            bundles.Add(new StyleBundle("~/bundles/style").Include(
                      "~/css/style.css",
                      "~/css/responsive.css"));

            bundles.Add(new StyleBundle("~/bundles/font").Include(
                     "~/css/fontawesome-all.min.css"));

            bundles.Add(new StyleBundle("~/bundles/vendorscss").Include(
                     "~/vendors/fancybox/jquery.fancybox.min.css",
                     "~/vendors/owl-carousel/assets/owl.carousel.min.css",
                     "~/vendors/arcticmodal/jquery.arcticmodal-0.3.css"));

            bundles.Add(new StyleBundle("~/bundles/vendorshomecss").Include(
                     "~/vendors/revolution/css/settings.css",
                     "~/vendors/revolution/css/layers.min.css",
                     "~/vendors/revolution/css/navigation.min.css"));
        }
    }
}
